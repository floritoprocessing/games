class Ship {
  
  float ACC_HMOV = 4000;
  float HMOV_SLOW_DOWN_FAC = 0.1;
  
  float FORWARD_ACC = 4000;
  float FORWARD_SLOW_DOWN_FAC = 0.03;
  
  PVector lastPos;
  PVector pos;
  
  float hMov = 0;
  float forwardMovement = 0;
  
  
  
  boolean doLeft = false;
  boolean doRight = false;
  
  PShape shape;
  float upDownOscilFreq;
  float rotateZFreq;
  float upDownOscilPhase;
  float rotateZPhase;
  
  Ship(float d, PVector pos) {
    this.lastPos = pos.copy();
    this.pos = pos.copy();
    float w = d/3;
    float h = d/7;
    shape = createShape();
    shape.beginShape(TRIANGLES);
    shape.fill(128);
    shape.stroke(255);
    shape.vertex(0,0,0);
    shape.vertex(w,0,d);
    shape.vertex(0,-h,d);
    shape.vertex(0,0,0);
    shape.vertex(-w,0,d);
    shape.vertex(0,-h,d);
    shape.endShape();
    upDownOscilFreq = random(3000,3200);
    upDownOscilPhase = random(TWO_PI);
    rotateZFreq = random(8000,9000);
    rotateZPhase = random(TWO_PI);
  }
  
  void left() {
    doLeft=true;
  }
  
  void right() {
    doRight=true;
  }
  
  
  void integrate(float frameTime) {
    lastPos.set(pos);
    
    if (doLeft) {
      hMov -= ACC_HMOV * frameTime;
      forwardMovement += FORWARD_ACC * frameTime;
    } 
    if (doRight) {
      hMov += ACC_HMOV * frameTime;
      forwardMovement += FORWARD_ACC * frameTime;
    }
    
    
    pos.x += hMov * frameTime;
    pos.z -= forwardMovement * frameTime;
    
    
    float reverseAcc = -hMov*HMOV_SLOW_DOWN_FAC/frameTime;
    hMov += reverseAcc * frameTime;
    
    reverseAcc = -forwardMovement*FORWARD_SLOW_DOWN_FAC / frameTime;
    forwardMovement += reverseAcc * frameTime;
     
    
    doLeft = doRight = false;
    
    
  }
  
  
  void draw() {
    pushMatrix();
    float yOff = map(sin( map(millis(),0,upDownOscilFreq,0,TWO_PI)+upDownOscilPhase ),-1,1,-5,5);
    translate(pos.x,pos.y+yOff,pos.z);
    float rotZ = map(sin( map(millis(),0,rotateZFreq,0,TWO_PI)+rotateZPhase ),-1,1,radians(-15),radians(15));
    rotateZ(rotZ);
    shape(shape); 
    popMatrix();
  }
  
}
