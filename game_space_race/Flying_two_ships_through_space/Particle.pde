abstract class Particle {
  
  PVector pos = new PVector();
  PVector mov = new PVector();
  
  Particle() {}
  
  
  Particle(PVector pos) {
    this.pos.set(pos);
  }
  
  Particle(PVector pos, PVector mov) {
    this.pos.set(pos);
    this.mov.set(mov);
  }
  
  void integrate(float time) {
    pos.add( PVector.mult(mov, time) );
  }
  
  abstract void draw();
  
}
