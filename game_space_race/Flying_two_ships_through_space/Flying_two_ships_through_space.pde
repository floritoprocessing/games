PShape circleShape;
ArrayList<ShapeParticle> circles;
int lastFrameTime;

float CAM_Z_LIMIT = 1000;

int INIT_PARTICLES = 5000;
int LIMIT_FPS = 45;
int DELTA_PARTICLES = 10;

FlyCamera camera;
Ship[] ships;

void setup() {
  //size(1920, 1080, P3D);
  fullScreen(P3D);

  camera = new FlyCamera(60);

  ships = new Ship[2];
  for (int i=0; i<2; i++) {
    ships[i] = new Ship(50, new PVector((i-0.5)*width*0.2, 30, CAM_Z_LIMIT*0.6));
  }

  circleShape = createCircle(6, 1.0);

  circles = new ArrayList<ShapeParticle>();
  lastFrameTime = millis();

  for (int i=0; i<INIT_PARTICLES; i++) {
    // create particle;
    PVector pos = new PVector(random(-1, 1)*width, random(-1, 1)*height, random(-CAM_Z_LIMIT, CAM_Z_LIMIT));
    PVector mov = createBasicSpeed();
    ShapeParticle circle = new ShapeParticle(circleShape, pos, mov);
    circles.add(circle);
  }

  frameRate(120);
}

void keyPressed() {
  if (key=='a') ships[0].left();
  if (key=='s') ships[0].right();
  
  if (key=='k') ships[1].left();
  if (key=='l') ships[1].right();
}

PVector createBasicSpeed() {
  return new PVector(0, 0, 0);//random(50));
}

void draw() {
  int ti = millis();
  float frameTime = (ti-lastFrameTime)*0.001;
  lastFrameTime = ti;

  if (frameRate>LIMIT_FPS) {
    for (int i=0; i<DELTA_PARTICLES; i++) {
      // create particle;
      PVector pos = new PVector(random(-1, 1)*width, random(-1, 1)*height, -CAM_Z_LIMIT);
      PVector mov = createBasicSpeed();
      ShapeParticle circle = new ShapeParticle(circleShape, pos, mov);
      circles.add(circle);
    }
  }
  if (frameRate<LIMIT_FPS && circles.size()>=DELTA_PARTICLES) {
    int lastIndex = circles.size()-1;
    for (int i=0; i<DELTA_PARTICLES; i++) {
      circles.remove(lastIndex-i);
    }
  }
  
  float maxDeltaZ = 0;
  for (Ship ship : ships) {
    ship.integrate(frameTime);
    float deltaZ = ship.lastPos.z-ship.pos.z;
    maxDeltaZ = max(maxDeltaZ, deltaZ);
  }
  
  // turn ship movement into star movement

  

  // integrate:
  for (ShapeParticle p : circles) {
    p.integrate(frameTime, CAM_Z_LIMIT, -CAM_Z_LIMIT);
  }
  


  // DRAWING DRAWING DRAWING

  background(0); 
  fill(255);

  beginCamera();

  camera.update();

  // draw:
  for (ShapeParticle p : circles) {
    p.draw();
  }

  lights();
  for (int i=0; i<2; i++) {
    ships[i].draw();
  }

  endCamera();

  float fov = radians(60);
  float cameraZ = (height/2.0) / tan(fov/2.0);
  perspective(fov, float(width)/float(height), 
    cameraZ/1000.0, cameraZ*1000.0);
  camera();

  fill(255);
  text("Particles: "+circles.size()+" @ "+nf(frameRate, 1, 1)+" fps", 10, 20);
}
