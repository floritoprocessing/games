class Ship {
  
  float ACC = 2000;
  float MAX_XY_SPEED = 10000;
  float XY_SLOW_DOWN_FAC = 0.1;
  float MAX_X_OFFSET = 250;
  float MAX_Y_OFFSET = 150;
  
  PVector steering = new PVector();
  PVector theSteer;
  PVector xyMov = new PVector();
  PVector xyPos = new PVector();
  PVector pos = new PVector();
  
  
  PShape shape;
  float upDownOscilFreq;
  float rotateZFreq;
  float upDownOscilPhase;
  float rotateZPhase;
  
  Ship(float d, PVector pos) {
    this.pos = pos.copy();
    float w = d/3;
    float h = d/7;
    shape = createShape();
    shape.beginShape(TRIANGLES);
    shape.fill(128);
    shape.stroke(255);
    shape.vertex(0,0,0);
    shape.vertex(w,0,d);
    shape.vertex(0,-h,d);
    shape.vertex(0,0,0);
    shape.vertex(-w,0,d);
    shape.vertex(0,-h,d);
    shape.endShape();
    upDownOscilFreq = random(3000,3200);
    upDownOscilPhase = random(TWO_PI);
    rotateZFreq = random(8000,9000);
    rotateZPhase = random(TWO_PI);
  }
  
  void steer(int direction, boolean on) {
    int val = on?1:-1;
    switch (direction) {
      case LEFT: steering.x-=val; break;
      case RIGHT: steering.x+=val; break;
      case UP: steering.y-=val; break;
      case DOWN: steering.y+=val; break;
    }
  }
  
  
  void integrate(float frameTime) {
    
    theSteer = PVector.mult( steering, ACC );
    xyMov.add ( PVector.mult( theSteer, frameTime ) );
    

    
    if (xyMov.mag()>MAX_XY_SPEED) {
      xyMov.setMag(MAX_XY_SPEED);
    }
    xyPos.add( PVector.mult( xyMov, frameTime ) );
    
    clip(xyPos, MAX_X_OFFSET, MAX_Y_OFFSET);
    
    PVector xySlowDown = PVector.mult( xyMov, -XY_SLOW_DOWN_FAC/frameTime);
    xyMov.add( PVector.mult( xySlowDown, frameTime ) );
    //float reverseAcc = -hMov*HMOV_SLOW_DOWN_FAC/frameTime;
    //hMov += reverseAcc * frameTime;

    
  }
  
  void clip(PVector vec, float maxXValue, float maxYValue) {
    if (vec.x>maxXValue) vec.x=maxXValue;
    else if (vec.x<-maxXValue) vec.x=-maxXValue;
    if (vec.y>maxYValue) vec.y=maxYValue;
    else if (vec.y<-maxYValue) vec.y=-maxYValue;
  }
  
  
  void draw() {
    
    float rotZ = 0;//map(theSteer.x,0,100
    //println(theSteer.x);
    
    pushMatrix();
    float yOff = map(sin( map(millis(),0,upDownOscilFreq,0,TWO_PI)+upDownOscilPhase ),-1,1,-5,5);
    translate(pos.x,pos.y+yOff,pos.z);
    translate(xyPos.x, xyPos.y);
    float rotZOscil = map(sin( map(millis(),0,rotateZFreq,0,TWO_PI)+rotateZPhase ),-1,1,radians(-15),radians(15));
    rotateZ(rotZOscil + rotZ);
    shape(shape); 
    popMatrix();
  }
  
}
