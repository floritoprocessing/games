class Stars {

  float CAM_Z_LIMIT = 1000;
  int INIT_PARTICLES = 5000;
  int LIMIT_FPS = 45;
  int DELTA_PARTICLES = 10;

  float BASE_SPEED = 400;

  PShape circleShape;
  ArrayList<ShapeParticle> circles;

  Stars() {
    reset();
  }

  void reset() {
    circleShape = createCircle(6, 1.0);
    circles = new ArrayList<ShapeParticle>();
    for (int i=0; i<INIT_PARTICLES; i++) {
      // create particle;
      PVector pos = new PVector(random(-1, 1)*width, random(-1, 1)*height, random(-CAM_Z_LIMIT, CAM_Z_LIMIT));
      PVector mov = createBasicSpeed();
      ShapeParticle circle = new ShapeParticle(circleShape, pos, mov);
      circles.add(circle);
    }
  }


  PVector createBasicSpeed() {
    return new PVector(0, 0, random(BASE_SPEED, BASE_SPEED+50));//random(50));
  }


  void integrate(float frameTime) {
    adaptNumberOfStars();

    // integrate:
    for (ShapeParticle p : circles) {
      p.integrate(frameTime, CAM_Z_LIMIT, -CAM_Z_LIMIT);
    }
  }



  void adaptNumberOfStars() {
    if (frameRate>LIMIT_FPS) {
      for (int i=0; i<DELTA_PARTICLES; i++) {
        // create particle;
        PVector pos = new PVector(random(-1, 1)*width, random(-1, 1)*height, -CAM_Z_LIMIT);
        PVector mov = createBasicSpeed();
        ShapeParticle circle = new ShapeParticle(circleShape, pos, mov);
        circles.add(circle);
      }
    }
    if (frameRate<LIMIT_FPS && circles.size()>=DELTA_PARTICLES) {
      int lastIndex = circles.size()-1;
      for (int i=0; i<DELTA_PARTICLES; i++) {
        circles.remove(lastIndex-i);
      }
    }
  }



  void draw() {
    // draw:
    for (ShapeParticle p : circles) {
      p.draw();
    }
  }
}
