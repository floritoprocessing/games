class FlyCamera {

  float fov = HALF_PI;
  float camY = 0;
  
  FlyCamera(float fovDeg) {
    fov = radians(fovDeg);
  }

  void draw() {
    
    float cameraZ = (height/2.0) / tan(fov/2.0);
    perspective(fov, float(width)/float(height), 
      cameraZ/1000.0, cameraZ*1000.0);
    camera(0, camY, 1000, 0, camY, 0, 0, 1, 0);
  }
}
