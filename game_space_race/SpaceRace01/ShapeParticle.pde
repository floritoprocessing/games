class ShapeParticle extends Particle {
  
  PShape shape;
  
  ShapeParticle(PShape shape) {
    super();
    this.shape = shape;
  }
  
  ShapeParticle(PShape shape, PVector pos) {
    super(pos);
    this.shape = shape;
  }
  
  ShapeParticle(PShape shape, PVector pos, PVector mov) {
    super(pos, mov);
    this.shape = shape;
  }
  
  void integrate(float frameTime, float maxZ, float minZ) {
    super.integrate(frameTime);
    if (pos.z>maxZ) pos.z=minZ;
  }
  
  void draw() {
    pushMatrix();
    translate(pos.x, pos.y, pos.z);
    shape(shape);
    popMatrix();
  }
  
}
