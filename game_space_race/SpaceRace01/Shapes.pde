PShape createCircle(int doubleTriangles, float radius) {
  
  PShape circle = createShape();
  circle.beginShape(TRIANGLE_STRIP);
  circle.fill(255);
  circle.noStroke();
  int triangles = doubleTriangles*2;
  int circlePoints = triangles+2;
  for (int i=0; i<circlePoints; i++) {
    float rd;
    int rdIndex = (i+1)/2;
    if (i==0) rd=0;
    else if (i==circlePoints-1) rd = PI;
    else rd = map(rdIndex, 0, triangles/2+1, 0, PI);
    rd *= (i%2==0?1:-1); // odd/even
    float x = radius * cos(rd);
    float y = radius * sin(rd);
    circle.vertex(x, y);
  }
  circle.endShape();
  
  return circle;
  
}
