
int lastFrameTime;

Stars stars;


FlyCamera camera;
Ship ship;

void setup() {
  //size(1920, 1080, P3D);
  fullScreen(P3D);
  frameRate(120);
  noCursor();

  camera = new FlyCamera(60);
  stars = new Stars();
  ship = new Ship(50, new PVector(0, 0, stars.CAM_Z_LIMIT*0.6));

  lastFrameTime = millis();
}


void keyPressed() {
  if (key==CODED) {
    if (keyCode==LEFT) ship.steer(LEFT, true);
    if (keyCode==RIGHT) ship.steer(RIGHT, true);
    if (keyCode==UP) ship.steer(UP, true);
    if (keyCode==DOWN) ship.steer(DOWN, true);
  }
}
void keyReleased() {
  if (key==CODED) {
    if (keyCode==LEFT) ship.steer(LEFT, false);
    if (keyCode==RIGHT) ship.steer(RIGHT, false);
    if (keyCode==UP) ship.steer(UP, false);
    if (keyCode==DOWN) ship.steer(DOWN, false);
  }
}


void draw() {
  int ti = millis();
  float frameTime = (ti-lastFrameTime)*0.001;
  lastFrameTime = ti;

  stars.integrate(frameTime);
  ship.integrate(frameTime);


  // DRAWING DRAWING DRAWING

  background(0); 
  fill(255);

  beginCamera();

  camera.draw();
  stars.draw();
  lights();
  ship.draw();

  endCamera();
}
