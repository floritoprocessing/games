class Player {
  
  int[] pos={0,0};
  float statusBarWidth=300;
  float statusBarHeight=15;
  float score=0;
  float scoreInc=0.01;
  boolean winner=false;
  
  Vector keys;
  
  Player(Vector k) {
    keys=new Vector(k);
  }
  
  void reset() {
    winner=false;
    score=0;
  }
  
  void setPosition(int x, int y) {
    pos[0]=x;
    pos[1]=y;
  }
  
  int getNrOfInputKeys() {
    return keys.size();
  }
  
  String getInputKey(int i) {
    return (String)(keys.elementAt(i));
  }
  
  void increaseScore() {
    score+=scoreInc;
    if (score>=1) {
      score=1;
      winner=true;
    }
  }
  
  boolean hasWon() {
    return winner;
  }
  
  void drawOnScreen() {
    rectMode(CORNER);
    stroke(0,0,0);
    fill(230,230,230);
    rect(pos[0],pos[1],statusBarWidth,statusBarHeight);
    if (!winner) stroke(0,0,0); else stroke(frameCount%2==0?0:255);
    if (!winner) fill(200,200,200); else fill(50,200,50);
    rect(pos[0],pos[1],score*statusBarWidth,statusBarHeight);
  }
}
