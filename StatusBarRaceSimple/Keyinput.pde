class Keyinput {
  
  Keyinput() {
  }
  
  void update(char c, Player[] p) {
    boolean updateNeeded=false;
    for (int i=0;i<p.length;i++) {
      for (int k=0;k<p[i].getNrOfInputKeys();k++) {
        if ((p[i].getInputKey(k)).equals(str(c))) {
          p[i].increaseScore();
          updateNeeded=true;
        }
      }
    }
    if (updateNeeded) redraw();
  }
}
