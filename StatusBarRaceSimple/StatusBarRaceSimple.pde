import java.util.Vector;

int PLAYERS_AMOUNT=2;

Keyinput keyinput;
Player[] player=new Player[PLAYERS_AMOUNT];
boolean isThereAWinner=false;

void setup() {
  size(400,300,P3D);
  keyinput=new Keyinput();
  for (int i=0;i<player.length;i++) {
    Vector keys=new Vector();
    keys.add(i==0?"q":"p");
    player[i]=new Player(keys);
    player[i].setPosition(50,50+i*50);
  }
}

void keyPressed() {
  if (!isThereAWinner) keyinput.update(key,player);
}

void draw() {
  background(240,240,245);
  for (int i=0;i<player.length;i++) {
    player[i].drawOnScreen();
    if (player[i].hasWon()) isThereAWinner=true;
  }
}
