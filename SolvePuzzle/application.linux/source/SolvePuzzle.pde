
Toggle next, next2, solve;
Knob kCol, kTurns; //kRow
Puzzle lastPuzzle=null, puzzleStart, puzzleEnd, puzzleSolution;
PFont font;

public static final int MODE_SETUP_SIZE=0;
public static final int MODE_SETUP_PUZZLE_START=1;
public static final int MODE_SETUP_PUZZLE_END=2;
public static final int MODE_SETUP_PUZZLE_TURNS=3;
public static final int MODE_SOLVE_PUZZLE=4;
public static final int MODE_END=5;

int MODE = MODE_SETUP_SIZE;

void setup() {
  size(800,600,P3D);
  font = loadFont("LucidaSans-Demi-12.vlw");
  textFont(font,12);
  kCol = new Knob(60,60,16,4,6,4,"size",font);
  kCol.stepSize(1);
  //kRow = new Knob(140,60,16,4,6,4,"rows",font);
  //kRow.stepSize(1);  
  kTurns = new Knob(610,60,16,1,3,1,"turns",font);
  kTurns.stepSize(1);
  
  next = new Toggle(280,60,30,"Next ->",font,color(192,180,33),color(64,55,4));
  next2 = new Toggle(580,60,30,"Next ->",font,color(192,180,33),color(64,55,4));
  solve = new Toggle(610,140,50,"Solve!",font,color(192,180,33),color(64,55,4));
  next.setTrigger();
  next2.setTrigger();
  solve.setTrigger();
  
  
}

void draw() {
  
  if (MODE==MODE_SETUP_SIZE) {
    background(0);
    fill(255);
    textAlign(LEFT);
    text("Setup Size: ",10,20);
    kCol.update();
    //kRow.update();
    next.update();
    if (next.isOn()) {
      if (lastPuzzle!=null&&lastPuzzle.getCol()==(int)kCol.getValue()&&lastPuzzle.getRow()==(int)kCol.getValue())
        puzzleStart = new Puzzle(puzzleStart);
      else
        puzzleStart = new Puzzle((int)kCol.getValue(),(int)kCol.getValue());
      lastPuzzle = new Puzzle(puzzleStart);
      MODE=MODE_SETUP_PUZZLE_START;
    }
  }
  
  if (MODE>=MODE_SETUP_PUZZLE_START&&MODE<=MODE_SOLVE_PUZZLE) {
    background(0);
    fill(255);
    textAlign(LEFT);
    text("Setup Puzzle start: ",10,20);
    puzzleStart.update();
    if (MODE==MODE_SETUP_PUZZLE_START) {
      next.update();
      if (next.isOn()) {
        puzzleEnd = new Puzzle(puzzleStart);
        puzzleEnd.shiftPosition(300,0);
        puzzleStart.noInteraction();
        MODE=MODE_SETUP_PUZZLE_END;
      }
    }
  }
  
  if (MODE>=MODE_SETUP_PUZZLE_END&&MODE<=MODE_SOLVE_PUZZLE) {
    fill(255);
    textAlign(LEFT);
    text("Setup Puzzle end: ",310,20);
    puzzleEnd.update();
    if (MODE==MODE_SETUP_PUZZLE_END) {
      next2.update();
      if (next2.isOn()) {
        puzzleEnd.noInteraction();
        MODE=MODE_SETUP_PUZZLE_TURNS;
      }
    }
  }
  
  if (MODE>=MODE_SETUP_PUZZLE_TURNS&&MODE<=MODE_SOLVE_PUZZLE) {
    fill(255);
    textAlign(LEFT);
    text("Setup Puzzle number of turns: ",510,20);
    kTurns.update();
    if (MODE==MODE_SETUP_PUZZLE_TURNS) {
      solve.update();
      if (solve.isOn()) {
        kTurns.noInteraction();
        MODE=MODE_SOLVE_PUZZLE;
        puzzleSolution = new Puzzle(puzzleStart);
        puzzleSolution.shiftPosition(260,280);
        puzzleSolution.noInteraction();
        puzzleSolution.initSolution((int)kTurns.getValue(),puzzleStart,puzzleEnd);
        frameRate(1000);
      }
    }
  }
  
  if (MODE==MODE_SOLVE_PUZZLE) {
    fill(255);
    textAlign(LEFT);
    text("Solving... tries left: "+puzzleSolution.getTriesLeft(),260,270);
    
    int triesPerFrame=50;
    int solved=Puzzle.SOLVING;
    while(triesPerFrame>0&&solved==Puzzle.SOLVING) {
      solved = puzzleSolution.tryOneSolution();
      puzzleSolution.update();
      triesPerFrame--;
    }
    
    fill(255);
    textAlign(LEFT);
    text((solved==Puzzle.SOLVING?"Solving":(solved==Puzzle.SOLVED?"Solved":"Not solved")),260,290);
    
    if (solved!=Puzzle.SOLVING) MODE=MODE_END;
  }
  
  if (MODE==MODE_END) {
    Toggle restart = new Toggle(500,400,50,"AGAIN!",font,color(192,180,33),color(64,55,4));
    restart.setTrigger();
    restart.update();
    if (restart.isOn()) {
      kTurns.interaction();
      MODE=MODE_SETUP_SIZE;
    }
  }
  
  
  
}
