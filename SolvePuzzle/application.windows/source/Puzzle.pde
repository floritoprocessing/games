class Puzzle {

  int col, row;      // nr of columns/rows
  Toggle[][] square;     // knobs

  Puzzle(int col, int row) {
    this.col=col;
    this.row=row;
    square = new Toggle[col][row];
    for (int y=0;y<row;y++) for (int x=0;x<col;x++) {
      square[x][y] = new Toggle(30+x*32,60+y*32,30,"",font,color(192,180,33),color(64,55,4));
    }
  }

  Puzzle(Puzzle p) {
    this(p.col,p.row);
    noHighlight();
    for (int y=0;y<row;y++) for (int x=0;x<col;x++)
      square[x][y].setState(p.square[x][y].isOn());
  }
  
  public int getCol() { return col; }
  public int getRow() { return row; }

  public void initState(Puzzle p) {
    for (int y=0;y<p.row;y++) for (int x=0;x<p.col;x++)
      square[x][y].setState(p.square[x][y].isOn());
  }

  public void noHighlight() {
    for (int y=0;y<row;y++) for (int x=0;x<col;x++)
      square[x][y].noHighlight();
  }

  public void shiftPosition(float xo, float yo) {
    for (int y=0;y<row;y++) for (int x=0;x<col;x++)
      square[x][y].shiftPosition(xo,yo);
  }

  public void noInteraction() {
    for (int y=0;y<row;y++) for (int x=0;x<col;x++)
      square[x][y].noInteraction();
  }

  void update() {
    for (int y=0;y<row;y++) for (int x=0;x<col;x++) {
      square[x][y].update();
    }
  }





  /*
  *    SOLVE!!!
   */

  public static final int SOLVING=0;
  public static final int SOLVED=1;
  public static final int NOT_SOLVED=2;

  int turns;
  int tries;
  boolean solved=false;
  Puzzle puzzleStart, puzzleEnd;
  int[] indexX, indexY; 

  public void initSolution(int turns, Puzzle puzzleStart, Puzzle puzzleEnd) {
    this.puzzleStart=puzzleStart;
    this.puzzleEnd=puzzleEnd;
    this.turns=turns;
    tries = (int)pow(col*row,turns);
    indexX = new int[turns];
    indexY = new int[turns];
    for (int i=0;i<indexX.length;i++) 
      indexX[i]=indexY[i]=0;
  }

  public int getTriesLeft() {
    return tries;
  }

  private void playPuzzle(int t, boolean blockIncrease) {
    t--;
    //print(indexX[t]+"/"+indexY[t]+"\t");
    square[indexX[t]][indexY[t]].highlight(""+(turns-t));
    
    for (int ixo=-1;ixo<=1;ixo++) for (int iyo=-1;iyo<=1;iyo++) {
      int ix = indexX[t]+ixo;
      int iy = indexY[t]+iyo;
      if (ix>=0&&ix<col&&iy>=0&&iy<row) {
        square[ix][iy].invertState();
      }
    }
    
    boolean nextLevelBlockIncrease=false;
    if (blockIncrease) {
      indexX[t]++;
      if (indexX[t]==col) {
        indexX[t]=0;
        indexY[t]++;
        if (indexY[t]==row) {
          indexY[t]=0;
          nextLevelBlockIncrease=true;
        }
      }
    }

    if (t>0) playPuzzle(t,nextLevelBlockIncrease);
  }
  
  boolean equalTo(Puzzle p) {
    for (int x=0;x<col;x++) for (int y=0;y<row;y++) {
      if (square[x][y].getState()!=p.square[x][y].getState()) return false;
    }
    return true;
  }
  

  public int tryOneSolution() {

    initState(puzzleStart);
    noHighlight();
    playPuzzle(turns,true);
    if (this.equalTo(puzzleEnd)) solved=true;

    tries--;
    if (tries<=0&&!solved)
      return NOT_SOLVED;
    else if (!solved)
      return SOLVING;
    else return SOLVED;
  }


}
