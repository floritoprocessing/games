class Puzzle {
  
  int col, row;      // nr of columns/rows
  Toggle[][] square;     // knobs
  
  Puzzle(int col, int row) {
    this.col=col;
    this.row=row;
    square = new Toggle[col][row];
    for (int y=0;y<row;y++) for (int x=0;x<col;x++) {
      square[x][y] = new Toggle(60+x*32,60+y*32,30,"",font,color(192,180,33),color(64,55,4));
    }
  }
  
  void update() {
    for (int y=0;y<row;y++) for (int x=0;x<col;x++) {
      square[x][y].update();
    }
  }
  
  
}
