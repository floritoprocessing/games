import processing.core.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; public class SolvePuzzle extends PApplet {
Toggle next;
Knob kCol, kRow;
Puzzle puzzleStart, puzzleEnd;
PFont font;

public static final int MODE_SETUP_SIZE=0;
public static final int MODE_SETUP_PUZZLE_START=1;
public static final int MODE_SETUP_PUZZLE_END=2;
int MODE = MODE_SETUP_SIZE;

public void setup() {
  size(800,600,P3D);
  font = loadFont("LucidaSans-Demi-12.vlw");
  textFont(font,12);
  kCol = new Knob(60,60,16,3,6,4,"columns",font);
  kRow = new Knob(140,60,16,3,6,4,"rows",font);
  next = new Toggle(220,60,30,"Next ->",font,color(192,180,33),color(64,55,4));
  next.setTrigger();
  
  kCol.stepSize(1);
  kRow.stepSize(1);  
}

public void draw() {
  
  if (MODE==MODE_SETUP_SIZE) {
    background(0);
    fill(255);
    textAlign(LEFT);
    text("Setup Sizes: ",10,20);
    kCol.update();
    kRow.update();
    next.update();
    if (next.isOn()) {
      puzzleStart = new Puzzle((int)kCol.getValue(),(int)kRow.getValue());
      MODE=MODE_SETUP_PUZZLE_START;
    }
  }
  
  else if (MODE==MODE_SETUP_PUZZLE_START||MODE==MODE_SETUP_PUZZLE_END) {
    background(0);
    fill(255);
    textAlign(LEFT);
    text("Setup Puzzle start: ",10,20);
    puzzleStart.update();
  }
  
  
  
}

class Knob {

  private float x,y;    // screen position
  private float r;      // radius
  private float v0, vr; // value min, range
  private String name;  // name
  private PFont font;   // font for display

  private float stepSize = 0;

  public static final int DRAG_MODE_HORIZONTAL = 0;
  public static final int DRAG_MODE_VERTICAL = 1;
  private int DRAG_MODE = DRAG_MODE_HORIZONTAL;

  private float EDGE = 2*PI/16.0f; // edge of knob turn

  // these variables are needed for mouseInterfacing
  private boolean lastMousePressed=false;
  private boolean changeValue=false;
  private float downX=0, downY=0, downValue=0, addValue=0;

  // the variable that holds the eventual value
  private float value;

  Knob(float x, float y, float r, float v0, float v1, float value, String name, PFont font) {
    this.x=x;
    this.y=y;
    this.r=r;
    this.v0=v0;
    this.vr=v1-v0;
    this.value=value;
    this.name=name;
    this.font=font;
  }
  
  public void stepSize(float stepSize) {
    this.stepSize=stepSize;
  }

  private float valueToRadians() {
    float vPerc = (getValue()-v0)/vr;
    float rad = vPerc * (2*PI - 2*EDGE) + PI/2.0f + EDGE;
    return rad;
  }

  private boolean mouseWithinKnob() {
    float dx = mouseX-x, dy = mouseY-y;
    return (dx*dx+dy*dy<r*r);
  }

  public void update() {
    // event mouse down:
    if (mouseWithinKnob()) {
      if (mousePressed&&!lastMousePressed) {
        changeValue=true;
        downValue=value;
        addValue=0;
        downX=mouseX;
        downY=mouseY;
      }
    }

    //event mouse up:
    if (!mousePressed&&lastMousePressed) {
      if (changeValue) {
        value = downValue + addValue;
        changeValue=false;
      }
    }
    lastMousePressed=mousePressed;

    if (changeValue&&mousePressed) {
      if (DRAG_MODE==DRAG_MODE_VERTICAL) {
        float dy=(downY-mouseY)/100.0f;
        //downY=mouseY;
        addValue = dy * vr;
      } 
      else if (DRAG_MODE==DRAG_MODE_HORIZONTAL) {
        float dx=(mouseX-downX)/100.0f;
        //downX=mouseX;
        addValue = dx * vr;
      }
      if (downValue+addValue<v0) addValue=v0-downValue;
      if (downValue+addValue>v0+vr) addValue=v0+vr-downValue;
    }

    // draw knob circle
    stroke(192);
    fill(128);
    ellipseMode(CENTER_RADIUS);
    ellipse(x,y,r,r);

    // draw line
    float rd = valueToRadians();
    line(x,y,x+r*cos(rd),y+r*sin(rd));

    // draw text
    textFont(font,12);
    textAlign(CENTER);
    fill(255);
    text(name+": "+nf(getValue(),1,2),x,y+2*r+3);
  }

  public float getValue() {
    float outVal=0;
    if (changeValue)
      outVal = downValue + addValue;
    else
      outVal = value;
    if (stepSize!=0) outVal=(outVal - outVal%stepSize);
    return outVal;
  }

}

class Puzzle {
  
  int col, row;      // nr of columns/rows
  Toggle[][] square;     // knobs
  
  Puzzle(int col, int row) {
    this.col=col;
    this.row=row;
    square = new Toggle[col][row];
    for (int y=0;y<row;y++) for (int x=0;x<col;x++) {
      square[x][y] = new Toggle(60+x*32,60+y*32,30,"",font,color(192,180,33),color(64,55,4));
    }
  }
  
  public void update() {
    for (int y=0;y<row;y++) for (int x=0;x<col;x++) {
      square[x][y].update();
    }
  }
  
  
}
class Toggle {
  
  private float x,y;                       // center
  private float s;                         // size
  private float[][] c = new float[2][2];   // corners
  private String name;                     // label
  private int colorOn, colorOff;         // colors
  private PFont font;                      // font
  
  private boolean lastMousePressed=false;
  
  private static final int MODE_TOGGLE=0;
  private static final int MODE_TRIGGER=1;
  private int MODE = MODE_TOGGLE;
  
  private boolean STATE = false;
  
  /**
  x/y:  center position
  s:    size
  **/
  Toggle(float x, float y, float s, String name, PFont font, int colorOn, int colorOff) {
    this.x=x;
    this.y=y;
    this.s=s;
    c[0][0]=x-s/2;
    c[0][1]=y-s/2;
    c[1][0]=x+s/2;
    c[1][1]=y+s/2;
    this.name=name;
    this.font=font;
    this.colorOn = colorOn;
    this.colorOff = colorOff;
  }
  
  public void setState(boolean state) {
    STATE=state;
  }
  
  public void setToggle() {
    MODE = MODE_TOGGLE;
  }
  
  public void setTrigger() {
    MODE = MODE_TRIGGER;
  }
  
  public void update() {
    if (MODE==MODE_TRIGGER) STATE=false;
    if (mousePressed&&!lastMousePressed) {
      // mouseDown:
      if (mouseX>=c[0][0]&&mouseX<=c[1][0]&&mouseY>=c[0][1]&&mouseY<=c[1][1]) 
        if (MODE==MODE_TOGGLE) STATE=!STATE;
        else if (MODE==MODE_TRIGGER) STATE=true;
    }
    else if (!mousePressed&&lastMousePressed) {
      // mouseUp:
      //if (MODE==MODE_TRIGGER) STATE=false;
    }
    lastMousePressed=mousePressed;
    
    rectMode(CORNERS);
    noStroke();
    fill(STATE?colorOn:colorOff);
    rect(c[0][0],c[0][1],c[1][0],c[1][1]);
    
    textFont(font,12);
    textAlign(CENTER);
    fill(255);
    text(name,x,y+s/2+12);
  }
  
  public boolean isOn() {
    return STATE;
  }
  
}
static public void main(String args[]) {   PApplet.main(new String[] { "SolvePuzzle" });}}