class Knob {

  private float x,y;    // screen position
  private float r;      // radius
  private float v0, vr; // value min, range
  private String name;  // name
  private PFont font;   // font for display

  private float stepSize = 0;

  public static final int DRAG_MODE_HORIZONTAL = 0;
  public static final int DRAG_MODE_VERTICAL = 1;
  private int DRAG_MODE = DRAG_MODE_HORIZONTAL;

  private float EDGE = 2*PI/16.0; // edge of knob turn

  // these variables are needed for mouseInterfacing
  private boolean lastMousePressed=false;
  private boolean changeValue=false;
  private float downX=0, downY=0, downValue=0, addValue=0;

  // the variable that holds the eventual value
  private float value;

  Knob(float x, float y, float r, float v0, float v1, float value, String name, PFont font) {
    this.x=x;
    this.y=y;
    this.r=r;
    this.v0=v0;
    this.vr=v1-v0;
    this.value=value;
    this.name=name;
    this.font=font;
  }
  
  public void stepSize(float stepSize) {
    this.stepSize=stepSize;
  }

  private float valueToRadians() {
    float vPerc = (getValue()-v0)/vr;
    float rad = vPerc * (2*PI - 2*EDGE) + PI/2.0 + EDGE;
    return rad;
  }

  private boolean mouseWithinKnob() {
    float dx = mouseX-x, dy = mouseY-y;
    return (dx*dx+dy*dy<r*r);
  }

  public void update() {
    // event mouse down:
    if (mouseWithinKnob()) {
      if (mousePressed&&!lastMousePressed) {
        changeValue=true;
        downValue=value;
        addValue=0;
        downX=mouseX;
        downY=mouseY;
      }
    }

    //event mouse up:
    if (!mousePressed&&lastMousePressed) {
      if (changeValue) {
        value = downValue + addValue;
        changeValue=false;
      }
    }
    lastMousePressed=mousePressed;

    if (changeValue&&mousePressed) {
      if (DRAG_MODE==DRAG_MODE_VERTICAL) {
        float dy=(downY-mouseY)/100.0;
        //downY=mouseY;
        addValue = dy * vr;
      } 
      else if (DRAG_MODE==DRAG_MODE_HORIZONTAL) {
        float dx=(mouseX-downX)/100.0;
        //downX=mouseX;
        addValue = dx * vr;
      }
      if (downValue+addValue<v0) addValue=v0-downValue;
      if (downValue+addValue>v0+vr) addValue=v0+vr-downValue;
    }

    // draw knob circle
    stroke(192);
    fill(128);
    ellipseMode(CENTER_RADIUS);
    ellipse(x,y,r,r);

    // draw line
    float rd = valueToRadians();
    line(x,y,x+r*cos(rd),y+r*sin(rd));

    // draw text
    textFont(font,12);
    textAlign(CENTER);
    fill(255);
    text(name+": "+nf(getValue(),1,2),x,y+2*r+3);
  }

  public float getValue() {
    float outVal=0;
    if (changeValue)
      outVal = downValue + addValue;
    else
      outVal = value;
    if (stepSize!=0) outVal=(outVal - outVal%stepSize);
    return outVal;
  }

}
