
Toggle next;
Knob kCol, kRow;
Puzzle puzzleStart, puzzleEnd;
PFont font;

public static final int MODE_SETUP_SIZE=0;
public static final int MODE_SETUP_PUZZLE_START=1;
public static final int MODE_SETUP_PUZZLE_END=2;
int MODE = MODE_SETUP_SIZE;

void setup() {
  size(800,600,P3D);
  font = loadFont("LucidaSans-Demi-12.vlw");
  textFont(font,12);
  kCol = new Knob(60,60,16,3,6,4,"columns",font);
  kRow = new Knob(140,60,16,3,6,4,"rows",font);
  next = new Toggle(220,60,30,"Next ->",font,color(192,180,33),color(64,55,4));
  next.setTrigger();
  
  kCol.stepSize(1);
  kRow.stepSize(1);  
}

void draw() {
  
  if (MODE==MODE_SETUP_SIZE) {
    background(0);
    fill(255);
    textAlign(LEFT);
    text("Setup Sizes: ",10,20);
    kCol.update();
    kRow.update();
    next.update();
    if (next.isOn()) {
      puzzleStart = new Puzzle((int)kCol.getValue(),(int)kRow.getValue());
      MODE=MODE_SETUP_PUZZLE_START;
    }
  }
  
  else if (MODE==MODE_SETUP_PUZZLE_START||MODE==MODE_SETUP_PUZZLE_END) {
    background(0);
    fill(255);
    textAlign(LEFT);
    text("Setup Puzzle start: ",10,20);
    puzzleStart.update();
  }
  
  
  
}
