class Toggle {

  private float x,y;                       // center
  private float s;                         // size
  private float[][] c = new float[2][2];   // corners
  private String name;                     // label
  private color colorOn, colorOff;         // colors
  private PFont font;                      // font
  private boolean highlight = false;       // border
  private String highlightText = "";       // highlight text

  private boolean interaction = true;      // react to mouse
  private boolean lastMousePressed=false;

  private static final int MODE_TOGGLE=0;
  private static final int MODE_TRIGGER=1;
  private int MODE = MODE_TOGGLE;

  private boolean STATE = false;

  /**
   * x/y:  center position
   * s:    size
   **/
  Toggle(float x, float y, float s, String name, PFont font, color colorOn, color colorOff) {
    this.x=x;
    this.y=y;
    this.s=s;
    c[0][0]=x-s/2;
    c[0][1]=y-s/2;
    c[1][0]=x+s/2;
    c[1][1]=y+s/2;
    this.name=name;
    this.font=font;
    this.colorOn = colorOn;
    this.colorOff = colorOff;
  }
  
  public float getX() {
    return x;
  }
  
  public float getY() {
    return y;
  }

  public PFont getFont() {
    return font;
  }
  
  public void setState(boolean state) {
    STATE=state;
  }
  
  public void invertState() {
    STATE=!STATE;
  }

  public void setToggle() {
    MODE = MODE_TOGGLE;
  }

  public void setTrigger() {
    MODE = MODE_TRIGGER;
  }

  public void noInteraction() {
    interaction=false;
  }

  public void interaction() {
    interaction=true;
  }
  
  public void highlight(String highlightText) {
    highlight();
    this.highlightText=highlightText;
  }
  
  public void highlight() {
    highlight=true;
  }
  
  public void noHighlight() {
    highlight=false;
  }

  public void shiftPosition(float xo, float yo) {
    x+=xo;
    y+=yo;
    c[0][0]+=xo;
    c[1][0]+=xo;
    c[0][1]+=yo;
    c[1][1]+=yo;
  }

  void update() {
    if (MODE==MODE_TRIGGER) STATE=false;

    if (interaction) {
      if (mousePressed&&!lastMousePressed) {
        // mouseDown:
        if (mouseX>=c[0][0]&&mouseX<=c[1][0]&&mouseY>=c[0][1]&&mouseY<=c[1][1]) 
          if (MODE==MODE_TOGGLE) STATE=!STATE;
          else if (MODE==MODE_TRIGGER) STATE=true;
      }
      else if (!mousePressed&&lastMousePressed) {
        // mouseUp:
        //if (MODE==MODE_TRIGGER) STATE=false;
      }
    }
    lastMousePressed=mousePressed;

    rectMode(CORNERS);
    if (highlight) {
      stroke(255);
    } else
      noStroke();
    fill(STATE?colorOn:colorOff);
    rect(c[0][0],c[0][1],c[1][0],c[1][1]);
    
    if (highlight) {
      noStroke();
      fill(255);
      textAlign(CENTER);
      text(highlightText,x,y);
    }

    textFont(font,12);
    textAlign(CENTER);
    fill(255);
    text(name,x,y+s/2+12);
  }

  public boolean isOn() {
    return STATE;
  }
  
  public boolean getState() {
    return STATE;
  }

}
