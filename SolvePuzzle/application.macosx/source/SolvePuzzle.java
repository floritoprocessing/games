import processing.core.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; public class SolvePuzzle extends PApplet {
Toggle next, next2, solve;
Knob kCol, kTurns; //kRow
Puzzle lastPuzzle=null, puzzleStart, puzzleEnd, puzzleSolution;
PFont font;

public static final int MODE_SETUP_SIZE=0;
public static final int MODE_SETUP_PUZZLE_START=1;
public static final int MODE_SETUP_PUZZLE_END=2;
public static final int MODE_SETUP_PUZZLE_TURNS=3;
public static final int MODE_SOLVE_PUZZLE=4;
public static final int MODE_END=5;

int MODE = MODE_SETUP_SIZE;

public void setup() {
  size(800,600,P3D);
  font = loadFont("LucidaSans-Demi-12.vlw");
  textFont(font,12);
  kCol = new Knob(60,60,16,4,6,4,"size",font);
  kCol.stepSize(1);
  //kRow = new Knob(140,60,16,4,6,4,"rows",font);
  //kRow.stepSize(1);  
  kTurns = new Knob(610,60,16,1,3,1,"turns",font);
  kTurns.stepSize(1);
  
  next = new Toggle(280,60,30,"Next ->",font,color(192,180,33),color(64,55,4));
  next2 = new Toggle(580,60,30,"Next ->",font,color(192,180,33),color(64,55,4));
  solve = new Toggle(610,140,50,"Solve!",font,color(192,180,33),color(64,55,4));
  next.setTrigger();
  next2.setTrigger();
  solve.setTrigger();
  
  
}

public void draw() {
  
  if (MODE==MODE_SETUP_SIZE) {
    background(0);
    fill(255);
    textAlign(LEFT);
    text("Setup Size: ",10,20);
    kCol.update();
    //kRow.update();
    next.update();
    if (next.isOn()) {
      if (lastPuzzle!=null&&lastPuzzle.getCol()==(int)kCol.getValue()&&lastPuzzle.getRow()==(int)kCol.getValue())
        puzzleStart = new Puzzle(puzzleStart);
      else
        puzzleStart = new Puzzle((int)kCol.getValue(),(int)kCol.getValue());
      lastPuzzle = new Puzzle(puzzleStart);
      MODE=MODE_SETUP_PUZZLE_START;
    }
  }
  
  if (MODE>=MODE_SETUP_PUZZLE_START&&MODE<=MODE_SOLVE_PUZZLE) {
    background(0);
    fill(255);
    textAlign(LEFT);
    text("Setup Puzzle start: ",10,20);
    puzzleStart.update();
    if (MODE==MODE_SETUP_PUZZLE_START) {
      next.update();
      if (next.isOn()) {
        puzzleEnd = new Puzzle(puzzleStart);
        puzzleEnd.shiftPosition(300,0);
        puzzleStart.noInteraction();
        MODE=MODE_SETUP_PUZZLE_END;
      }
    }
  }
  
  if (MODE>=MODE_SETUP_PUZZLE_END&&MODE<=MODE_SOLVE_PUZZLE) {
    fill(255);
    textAlign(LEFT);
    text("Setup Puzzle end: ",310,20);
    puzzleEnd.update();
    if (MODE==MODE_SETUP_PUZZLE_END) {
      next2.update();
      if (next2.isOn()) {
        puzzleEnd.noInteraction();
        MODE=MODE_SETUP_PUZZLE_TURNS;
      }
    }
  }
  
  if (MODE>=MODE_SETUP_PUZZLE_TURNS&&MODE<=MODE_SOLVE_PUZZLE) {
    fill(255);
    textAlign(LEFT);
    text("Setup Puzzle number of turns: ",510,20);
    kTurns.update();
    if (MODE==MODE_SETUP_PUZZLE_TURNS) {
      solve.update();
      if (solve.isOn()) {
        kTurns.noInteraction();
        MODE=MODE_SOLVE_PUZZLE;
        puzzleSolution = new Puzzle(puzzleStart);
        puzzleSolution.shiftPosition(260,280);
        puzzleSolution.noInteraction();
        puzzleSolution.initSolution((int)kTurns.getValue(),puzzleStart,puzzleEnd);
        frameRate(1000);
      }
    }
  }
  
  if (MODE==MODE_SOLVE_PUZZLE) {
    fill(255);
    textAlign(LEFT);
    text("Solving... tries left: "+puzzleSolution.getTriesLeft(),260,270);
    
    int triesPerFrame=50;
    int solved=Puzzle.SOLVING;
    while(triesPerFrame>0&&solved==Puzzle.SOLVING) {
      solved = puzzleSolution.tryOneSolution();
      puzzleSolution.update();
      triesPerFrame--;
    }
    
    fill(255);
    textAlign(LEFT);
    text((solved==Puzzle.SOLVING?"Solving":(solved==Puzzle.SOLVED?"Solved":"Not solved")),260,290);
    
    if (solved!=Puzzle.SOLVING) MODE=MODE_END;
  }
  
  if (MODE==MODE_END) {
    Toggle restart = new Toggle(500,400,50,"AGAIN!",font,color(192,180,33),color(64,55,4));
    restart.setTrigger();
    restart.update();
    if (restart.isOn()) {
      kTurns.interaction();
      MODE=MODE_SETUP_SIZE;
    }
  }
  
  
  
}

class Knob {

  private float x,y;    // screen position
  private float r;      // radius
  private float v0, vr; // value min, range
  private String name;  // name
  private PFont font;   // font for display

  private float stepSize = 0;

  public static final int DRAG_MODE_HORIZONTAL = 0;
  public static final int DRAG_MODE_VERTICAL = 1;
  private int DRAG_MODE = DRAG_MODE_HORIZONTAL;

  private float EDGE = 2*PI/16.0f; // edge of knob turn

  // these variables are needed for mouseInterfacing
  private boolean interaction=true;
  private boolean lastMousePressed=false;
  private boolean changeValue=false;
  private float downX=0, downY=0, downValue=0, addValue=0;

  // the variable that holds the eventual value
  private float value;

  Knob(float x, float y, float r, float v0, float v1, float value, String name, PFont font) {
    this.x=x;
    this.y=y;
    this.r=r;
    this.v0=v0;
    this.vr=v1-v0;
    this.value=value;
    this.name=name;
    this.font=font;
  }

  public void stepSize(float stepSize) {
    this.stepSize=stepSize;
  }

  public void noInteraction() {
    interaction=false;
  }

  public void interaction() {
    interaction=true;
  }

  private float valueToRadians() {
    float vPerc = (getValue()-v0)/vr;
    float rad = vPerc * (2*PI - 2*EDGE) + PI/2.0f + EDGE;
    return rad;
  }

  private boolean mouseWithinKnob() {
    float dx = mouseX-x, dy = mouseY-y;
    return (dx*dx+dy*dy<r*r);
  }

  public void update() {
    if (interaction) {
      // event mouse down:
      if (mouseWithinKnob()) {
        if (mousePressed&&!lastMousePressed) {
          changeValue=true;
          downValue=value;
          addValue=0;
          downX=mouseX;
          downY=mouseY;
        }
      }

      //event mouse up:
      if (!mousePressed&&lastMousePressed) {
        if (changeValue) {
          value = downValue + addValue;
          changeValue=false;
        }
      }


      if (changeValue&&mousePressed) {
        if (DRAG_MODE==DRAG_MODE_VERTICAL) {
          float dy=(downY-mouseY)/100.0f;
          //downY=mouseY;
          addValue = dy * vr;
        } 
        else if (DRAG_MODE==DRAG_MODE_HORIZONTAL) {
          float dx=(mouseX-downX)/100.0f;
          //downX=mouseX;
          addValue = dx * vr;
        }
        if (downValue+addValue<v0) addValue=v0-downValue;
        if (downValue+addValue>v0+vr) addValue=v0+vr-downValue;
      }
    }
    lastMousePressed=mousePressed;

    // draw knob circle
    stroke(192);
    fill(128);
    ellipseMode(CENTER_RADIUS);
    ellipse(x,y,r,r);

    // draw line
    float rd = valueToRadians();
    line(x,y,x+r*cos(rd),y+r*sin(rd));

    // draw text
    textFont(font,12);
    textAlign(CENTER);
    fill(255);
    text(name+": "+nf(getValue(),1,2),x,y+2*r+3);
  }

  public float getValue() {
    float outVal=0;
    if (changeValue)
      outVal = downValue + addValue;
    else
      outVal = value;
    if (stepSize!=0) outVal=(outVal - outVal%stepSize);
    return outVal;
  }

}

class Puzzle {

  int col, row;      // nr of columns/rows
  Toggle[][] square;     // knobs

  Puzzle(int col, int row) {
    this.col=col;
    this.row=row;
    square = new Toggle[col][row];
    for (int y=0;y<row;y++) for (int x=0;x<col;x++) {
      square[x][y] = new Toggle(30+x*32,60+y*32,30,"",font,color(192,180,33),color(64,55,4));
    }
  }

  Puzzle(Puzzle p) {
    this(p.col,p.row);
    noHighlight();
    for (int y=0;y<row;y++) for (int x=0;x<col;x++)
      square[x][y].setState(p.square[x][y].isOn());
  }
  
  public int getCol() { return col; }
  public int getRow() { return row; }

  public void initState(Puzzle p) {
    for (int y=0;y<p.row;y++) for (int x=0;x<p.col;x++)
      square[x][y].setState(p.square[x][y].isOn());
  }

  public void noHighlight() {
    for (int y=0;y<row;y++) for (int x=0;x<col;x++)
      square[x][y].noHighlight();
  }

  public void shiftPosition(float xo, float yo) {
    for (int y=0;y<row;y++) for (int x=0;x<col;x++)
      square[x][y].shiftPosition(xo,yo);
  }

  public void noInteraction() {
    for (int y=0;y<row;y++) for (int x=0;x<col;x++)
      square[x][y].noInteraction();
  }

  public void update() {
    for (int y=0;y<row;y++) for (int x=0;x<col;x++) {
      square[x][y].update();
    }
  }





  /*
  *    SOLVE!!!
   */

  public static final int SOLVING=0;
  public static final int SOLVED=1;
  public static final int NOT_SOLVED=2;

  int turns;
  int tries;
  boolean solved=false;
  Puzzle puzzleStart, puzzleEnd;
  int[] indexX, indexY; 

  public void initSolution(int turns, Puzzle puzzleStart, Puzzle puzzleEnd) {
    this.puzzleStart=puzzleStart;
    this.puzzleEnd=puzzleEnd;
    this.turns=turns;
    tries = (int)pow(col*row,turns);
    indexX = new int[turns];
    indexY = new int[turns];
    for (int i=0;i<indexX.length;i++) 
      indexX[i]=indexY[i]=0;
  }

  public int getTriesLeft() {
    return tries;
  }

  private void playPuzzle(int t, boolean blockIncrease) {
    t--;
    //print(indexX[t]+"/"+indexY[t]+"\t");
    square[indexX[t]][indexY[t]].highlight(""+(turns-t));
    
    for (int ixo=-1;ixo<=1;ixo++) for (int iyo=-1;iyo<=1;iyo++) {
      int ix = indexX[t]+ixo;
      int iy = indexY[t]+iyo;
      if (ix>=0&&ix<col&&iy>=0&&iy<row) {
        square[ix][iy].invertState();
      }
    }
    
    boolean nextLevelBlockIncrease=false;
    if (blockIncrease) {
      indexX[t]++;
      if (indexX[t]==col) {
        indexX[t]=0;
        indexY[t]++;
        if (indexY[t]==row) {
          indexY[t]=0;
          nextLevelBlockIncrease=true;
        }
      }
    }

    if (t>0) playPuzzle(t,nextLevelBlockIncrease);
  }
  
  public boolean equalTo(Puzzle p) {
    for (int x=0;x<col;x++) for (int y=0;y<row;y++) {
      if (square[x][y].getState()!=p.square[x][y].getState()) return false;
    }
    return true;
  }
  

  public int tryOneSolution() {

    initState(puzzleStart);
    noHighlight();
    playPuzzle(turns,true);
    if (this.equalTo(puzzleEnd)) solved=true;

    tries--;
    if (tries<=0&&!solved)
      return NOT_SOLVED;
    else if (!solved)
      return SOLVING;
    else return SOLVED;
  }


}

class Toggle {

  private float x,y;                       // center
  private float s;                         // size
  private float[][] c = new float[2][2];   // corners
  private String name;                     // label
  private int colorOn, colorOff;         // colors
  private PFont font;                      // font
  private boolean highlight = false;       // border
  private String highlightText = "";       // highlight text

  private boolean interaction = true;      // react to mouse
  private boolean lastMousePressed=false;

  private static final int MODE_TOGGLE=0;
  private static final int MODE_TRIGGER=1;
  private int MODE = MODE_TOGGLE;

  private boolean STATE = false;

  /**
   * x/y:  center position
   * s:    size
   **/
  Toggle(float x, float y, float s, String name, PFont font, int colorOn, int colorOff) {
    this.x=x;
    this.y=y;
    this.s=s;
    c[0][0]=x-s/2;
    c[0][1]=y-s/2;
    c[1][0]=x+s/2;
    c[1][1]=y+s/2;
    this.name=name;
    this.font=font;
    this.colorOn = colorOn;
    this.colorOff = colorOff;
  }
  
  public float getX() {
    return x;
  }
  
  public float getY() {
    return y;
  }

  public PFont getFont() {
    return font;
  }
  
  public void setState(boolean state) {
    STATE=state;
  }
  
  public void invertState() {
    STATE=!STATE;
  }

  public void setToggle() {
    MODE = MODE_TOGGLE;
  }

  public void setTrigger() {
    MODE = MODE_TRIGGER;
  }

  public void noInteraction() {
    interaction=false;
  }

  public void interaction() {
    interaction=true;
  }
  
  public void highlight(String highlightText) {
    highlight();
    this.highlightText=highlightText;
  }
  
  public void highlight() {
    highlight=true;
  }
  
  public void noHighlight() {
    highlight=false;
  }

  public void shiftPosition(float xo, float yo) {
    x+=xo;
    y+=yo;
    c[0][0]+=xo;
    c[1][0]+=xo;
    c[0][1]+=yo;
    c[1][1]+=yo;
  }

  public void update() {
    if (MODE==MODE_TRIGGER) STATE=false;

    if (interaction) {
      if (mousePressed&&!lastMousePressed) {
        // mouseDown:
        if (mouseX>=c[0][0]&&mouseX<=c[1][0]&&mouseY>=c[0][1]&&mouseY<=c[1][1]) 
          if (MODE==MODE_TOGGLE) STATE=!STATE;
          else if (MODE==MODE_TRIGGER) STATE=true;
      }
      else if (!mousePressed&&lastMousePressed) {
        // mouseUp:
        //if (MODE==MODE_TRIGGER) STATE=false;
      }
    }
    lastMousePressed=mousePressed;

    rectMode(CORNERS);
    if (highlight) {
      stroke(255);
    } else
      noStroke();
    fill(STATE?colorOn:colorOff);
    rect(c[0][0],c[0][1],c[1][0],c[1][1]);
    
    if (highlight) {
      noStroke();
      fill(255);
      textAlign(CENTER);
      text(highlightText,x,y);
    }

    textFont(font,12);
    textAlign(CENTER);
    fill(255);
    text(name,x,y+s/2+12);
  }

  public boolean isOn() {
    return STATE;
  }
  
  public boolean getState() {
    return STATE;
  }

}
static public void main(String args[]) {   PApplet.main(new String[] { "SolvePuzzle" });}}