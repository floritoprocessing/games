package graf.tron;

import processing.core.PApplet;

public class TronGameIntro {

	private boolean introFinished = false;
	private float introTime;// = TronSettings.INTRO_TIME;
	private final long startTime;

	private final PApplet pa;
	
	public TronGameIntro(PApplet pa) {
		this.pa = pa;
		startTime = System.currentTimeMillis();
	}

	public float intro() {
		introTime = System.currentTimeMillis()-startTime;
		//introTime-=frameTimeMs;
	
		if (introTime>TronSettings.INTRO_TIME) {
			introTime=TronSettings.INTRO_TIME;
			introFinished=true;
		}
	
		/*
		 * GRID
		 */
		
		float negIntroPreparePercentage = 1-((float)introTime/TronSettings.INTRO_GRID_TIME);
		if (negIntroPreparePercentage<0) negIntroPreparePercentage=0;
		TronBackground.draw(pa,negIntroPreparePercentage);
	
		/*
		 * READY?
		 */
		
		
		float preparePercentage = (introTime-TronSettings.INTRO_GRID_TIME)/TronSettings.INTRO_PREPARE_TIME;
		if (preparePercentage>0) {
			pa.textAlign(PApplet.CENTER);
			int opac = 0x20 + (int)(0xb0 * preparePercentage);
			pa.fill(opac<<24 | 0x00ffffff&TronSettings.INTRO_READY_COLOR);
			pa.text("READY?",pa.width/2,pa.height/2);
		}
		
		
		
		return (float)introTime/TronSettings.INTRO_TIME;
	}

	public boolean introFinished() {
		return introFinished ;
	}

}
