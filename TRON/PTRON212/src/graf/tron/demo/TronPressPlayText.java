package graf.tron.demo;

import processing.core.PApplet;
import graf.tron.PTron;
import graf.tron.TronPlayerSettings;
import graf.tron.timer.TimerEventListener;

public class TronPressPlayText implements TimerEventListener {

	private final PTron tron;

	public TronPressPlayText(PTron tron) {
		this.tron=tron;
	}

	@Override
	public String getName() {
		return "TronDemoText";
	}

	@Override
	public void update(float firingPercentage) {
		if (System.currentTimeMillis()%300<150) {
			tron.textFont(tron.font18);
			tron.textAlign(PApplet.CENTER);
			tron.fill(TronPlayerSettings.drawColor[0]);
			
			int pos = (int)(firingPercentage*8)%4;
			
			tron.pushMatrix();
			tron.translate(((pos==0||pos==2)?tron.width/2:(pos==1?20:tron.width-20)),
					(pos==0?tron.height-20:((pos==1||pos==3)?tron.height/2:20)));
			tron.rotate(pos*PApplet.HALF_PI);
			tron.text("Press a button to play",0,0);
			tron.popMatrix();
			
			
		}
	}

}
