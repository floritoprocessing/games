package graf.tron.demo;

import processing.core.PApplet;
import graf.tron.PTron;
import graf.tron.TronPlayerSettings;
import graf.tron.timer.TimerEventListener;

public class TronTitleText implements TimerEventListener {

	private final PTron tron;
	private final String title = "Pixel Cycle";
	private final String subtitle = "A game by LosObsoletos.net";
	
	public TronTitleText(PTron tron) {
		this.tron=tron;
	}
	
	@Override
	public String getName() {
		return "TITLETEXT";
	}

	@Override
	public void update(float firingPercentage) {
		int opac1 = (int)(255 * (firingPercentage<0.9?1:(1-(firingPercentage-0.9)*10)));
		int opac2 = (int)(160 * (firingPercentage<0.9?1:(1-(firingPercentage-0.9)*10)));
		
		tron.pushMatrix();
		tron.textFont(tron.font72);
		tron.textAlign(PApplet.CENTER);
		tron.fill(opac1<<24 | 0x00fffff&TronPlayerSettings.drawColor[2]);
		
		float distance = Math.min(1,firingPercentage*20);
		
		tron.translate(tron.width/2,tron.height/2,-2500*(1-distance) + (firingPercentage<0.9?0:((firingPercentage-0.9f)*3000)));
		tron.text(title,0,0);
		
		
		if (firingPercentage>0.15) {
			tron.fill(opac2<<24|0x00ffffff&TronPlayerSettings.drawColor[2]);
			tron.textFont(tron.font24);
			tron.textAlign(PApplet.LEFT);
			tron.text(subtitle.substring(0,(int)(subtitle.length()*Math.min(1,(firingPercentage-0.15)*4))),-200,50);
		}
		
		tron.popMatrix();
	}

}
