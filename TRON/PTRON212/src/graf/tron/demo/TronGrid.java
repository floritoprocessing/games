package graf.tron.demo;

import processing.core.PApplet;
import graf.tron.PTron;
import graf.tron.TronPlayerSettings;
import graf.tron.timer.TimerEventListener;
import graf.tron.TronSettings;

public class TronGrid implements TimerEventListener {

	private final PTron tron;
	
	//float rotX = (float)(0.05*Math.PI);
	//float rotY = 0;
	float xOffset = 0;
	
	Racer r0 = new Racer(-1);
	Racer r1 = new Racer(0);
	Racer r2 = new Racer(1);
	
	public TronGrid(PTron tron) {
		this.tron = tron;
	}

	public String getName() {
		return "TronGrid";
	}
	
	private class Racer {
		
		int type=0; // -1, 0, 1
		
		Racer(int type) {
			this.type=type;
		}
		
		private void line(float x0, float x1, float y0, float y1, float z0, float z1) {
			tron.beginShape(PApplet.QUADS);
			tron.vertex(x0,y0,z0);
			tron.vertex(x0,y0,z1);
			tron.vertex(x1,y1,z1);
			tron.vertex(x1,y1,z0);
			tron.endShape();
		}
		
		void draw(int opac, float firingPercentage) {
			tron.stroke(opac<<24 | 0x00ffffff&TronPlayerSettings.drawColor[type+1]);
			tron.fill((opac/2)<<24 | 0x00ffffff&TronPlayerSettings.drawColor[type+1]);
			float line1Perc = Math.min(1,firingPercentage*2);
			float line2Perc = Math.max(0,firingPercentage*2-1);
			float x0=type*50, x1=type*50;
			float y0=-tron.height, y1=-tron.height+line1Perc*tron.height;
			
			line(x0,x1,y0,y1,0,10);
			
			if (line2Perc>0) {
				x0=type*50;
				x1=type*50 + type*line2Perc*(tron.height-50);
				y0=0;
				y1=(1-Math.abs(type))*tron.height*line2Perc;
				line(x0,x1,y0,y1,0,10);
			}
		}
	}

	public void update(float firingPercentage) {
		tron.pushMatrix();
		float distance = -100-(float)((1-Math.cos(2*Math.PI*firingPercentage))*0.3*500);
		float xOffset = (float)(tron.width/6*Math.sin(2*Math.PI*firingPercentage));
		tron.translate(tron.width/2+xOffset, tron.height/2, distance);

		double rotX = PApplet.PI*(0.1+firingPercentage*0.1);
		tron.rotateX((float)rotX);
		tron.pushMatrix();		
		tron.rotateZ(PApplet.PI+1.2f*firingPercentage);
		
		int opac = (int)(128*Math.min(1,3*Math.sin(firingPercentage*Math.PI)));
		tron.stroke(opac<<24|0x00ffffff&TronSettings.BACKGROUND_GRIDCOLOR);
		
		int xSteps = (int)(tron.width*2/TronSettings.BACKGROUND_GRIDSIZE);
		int ySteps = (int)(tron.height*2/TronSettings.BACKGROUND_GRIDSIZE);
		for (int x=0;x<=xSteps;x++) {
			float xx = x*TronSettings.BACKGROUND_GRIDSIZE - tron.width;
			tron.line(xx, -tron.height, xx, -tron.height+ySteps*TronSettings.BACKGROUND_GRIDSIZE);
		}
		for (int y=0;y<=ySteps;y++) {
			float yy = y*TronSettings.BACKGROUND_GRIDSIZE - tron.height;
			tron.line(-tron.width, yy, -tron.width+xSteps*TronSettings.BACKGROUND_GRIDSIZE, yy);
		}
		
		r0.draw(opac,firingPercentage);
		r1.draw(opac,firingPercentage);
		r2.draw(opac,firingPercentage);
		
		tron.popMatrix();
		tron.popMatrix();
	}

}
