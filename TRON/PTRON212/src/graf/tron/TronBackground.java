package graf.tron;

import graf.pimage.GImage;
import processing.core.PApplet;

public class TronBackground {

	public static void draw(PApplet pa, float negIntroPercentage) {

		float introPercentage = 1-negIntroPercentage;

		if (negIntroPercentage==0) {
			pa.background(0);
			drawFullGrid(pa);
		} else {

			int width = pa.width;
			int height = pa.height;

			int val = (int)(255*Math.pow(negIntroPercentage,8));
			float opac = TronSettings.BACKGROUND_GRID_OPACITY*introPercentage;
			float opac2 = opac*0.5f;
			pa.background(val);
			for (int x=0;x<width;x+=TronSettings.BACKGROUND_GRIDSIZE) {
				for (int y=0;y<height*introPercentage;y++) {
					GImage.set(pa,GImage.MODE_ADD,x-1, y, TronSettings.BACKGROUND_GRIDCOLOR, opac2);//, opac2);
					GImage.set(pa,GImage.MODE_ADD,x, y, TronSettings.BACKGROUND_GRIDCOLOR, opac);//, opac);
					GImage.set(pa,GImage.MODE_ADD,x+1, y, TronSettings.BACKGROUND_GRIDCOLOR, opac2);//, opac2);
				}
			}
			for (int y=0;y<height;y+=TronSettings.BACKGROUND_GRIDSIZE) {
				for (int x=0;x<width*introPercentage;x++) {
					GImage.set(pa,GImage.MODE_ADD,x, y-1, TronSettings.BACKGROUND_GRIDCOLOR, opac2);//, opac2);
					GImage.set(pa,GImage.MODE_ADD,x, y, TronSettings.BACKGROUND_GRIDCOLOR, opac);//, opac);
					GImage.set(pa,GImage.MODE_ADD,x, y+1, TronSettings.BACKGROUND_GRIDCOLOR, opac2);//, opac2);
				}
			}
		}
	}

	public static void drawFullGrid(PApplet pa) {
		int width = pa.width;
		int height = pa.height;
		float opac = TronSettings.BACKGROUND_GRID_OPACITY;
		float opac2 = TronSettings.BACKGROUND_GRID_OPACITY*0.5f;

		for (int x=0;x<width;x+=TronSettings.BACKGROUND_GRIDSIZE) {
			for (int y=0;y<height;y++) {
				GImage.set(pa,GImage.MODE_ADD,x-1, y, TronSettings.BACKGROUND_GRIDCOLOR, opac2);
				GImage.set(pa,GImage.MODE_ADD,x, y, TronSettings.BACKGROUND_GRIDCOLOR, opac);
				GImage.set(pa,GImage.MODE_ADD,x+1, y, TronSettings.BACKGROUND_GRIDCOLOR, opac2);
			}
		}
		for (int y=0;y<height;y+=TronSettings.BACKGROUND_GRIDSIZE) {
			for (int x=0;x<width;x++) {
				GImage.set(pa,GImage.MODE_ADD,x, y-1, TronSettings.BACKGROUND_GRIDCOLOR, opac2);
				GImage.set(pa,GImage.MODE_ADD,x, y, TronSettings.BACKGROUND_GRIDCOLOR, opac);
				GImage.set(pa,GImage.MODE_ADD,x, y+1, TronSettings.BACKGROUND_GRIDCOLOR, opac2);
			}
		}
	}

}
