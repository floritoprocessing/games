package graf.tron;

import graf.pimage.GImage;

import java.util.Vector;

import processing.core.PApplet;

public class Smear {

	private final float speed = TronSettings.SMEAR_SPEED_MIN + (float)(TronSettings.SMEAR_SPEED_VARIATION*Math.random());
	private float opacity = TronSettings.SMEAR_BASE_OPACITY;
	private final int color;
	private final float xm, ym;
	private float x, y;
	private float life=1.0f;
	private boolean dead = false;

	public Smear(float x, float y, float smearDirection, int color, float extra) {
		this.x=x;
		this.y=y;
		this.color = color;
		opacity += extra;
		if (opacity>1) opacity=1;
		xm = (extra+speed)*(float)Math.cos(smearDirection);
		ym = (extra+speed)*(float)Math.sin(smearDirection);
	}
	
	public void shiftPosOutward(float len) {
		double fac = len/Math.sqrt(xm*xm+ym*ym);
		x += xm*fac;
		y += ym*fac;
	}

	public void moveAndDraw(PApplet pa, GImage img) {
		x+=xm;
		y+=ym;
		life-=TronSettings.SMEAR_AGE_SPEED;
		if (life>0) {
			GImage.set(pa,GImage.MODE_ADD,x,y,color,life*opacity);
			if (img!=null) {
				img.setBlendMode(GImage.MODE_ADD);
				img.set(x,y,color,life*opacity);
			}
		} else {
			dead=true;
		}
	}

	public static Vector<Smear> createSmearDot(float x, float y, int color, float extra) {
		Vector<Smear> out = new Vector<Smear>();
		int degstep = 15 - (int)(10*extra);
		for (int i=0;i<360;i+=degstep) {
			float dr = (i+(int)(Math.random()*degstep)) * (float)Math.PI/180.0f;
			out.add(new Smear(x,y,dr,color,extra));
		}
		return out;
	}

	public static Vector<Smear> createSmearLine(float x0, float y0, float x1, float y1, float movX, float movY, int color, float extra) {
		Vector<Smear> out = new Vector<Smear>();
		float dx=x1-x0;
		float dy=y1-y0;
		double d = Math.sqrt(dx*dx+dy*dy);

		for (double dd=0;dd<d;dd++) {
			double p = dd/d;
			double x = x0+p*dx;
			double y = y0+p*dy;
			out.addAll(createSmearDot((float)x,(float)y,color,extra));
		}

		return out;
	}

	public boolean hasDied() {
		return dead;
	}

	public static Vector<Smear> createSmearDot(TronPlayer player, float extra) {
		return createSmearDot(player.getX(),player.getY(),player.getColor(),extra);
	}

}
