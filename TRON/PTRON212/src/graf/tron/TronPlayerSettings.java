package graf.tron;

import java.awt.event.KeyEvent;

public class TronPlayerSettings {
	
	// r b g
	public static final int[] drawColor = {
		0xfff0200e,
		0xff020ef0, //0xff020ef0,
		0xff0ef002
	};
	
	public static final int[] PLAYER_TEXT_COLOR = {
		0xfff0200e,
		0xff1070ff, //0xff020ef0,
		0xff0ef002
	};
	
	public static final int[][] steeringKey = {
		{ KeyEvent.VK_A, KeyEvent.VK_S },
		{ KeyEvent.VK_O, KeyEvent.VK_P },
		{ KeyEvent.VK_4, KeyEvent.VK_5 }
	};
}
