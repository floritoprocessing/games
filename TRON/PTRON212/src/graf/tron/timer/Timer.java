package graf.tron.timer;


import java.util.Vector;

public class Timer {

	private long startTime;
	private boolean running = false;
	private final int delay;
	private Vector<TimerEvent> events = new Vector<TimerEvent>();
	private boolean looping = false;
	
	public Timer(int delay) {
		this.delay=delay;
	}
	
	public Timer(int delay, boolean looping) {
		this.delay=delay;
		this.looping = looping;
	}
	
	public String[] getFiredEventNames() {
		TimerEvent[] outEvents = getFiredEvents();
		String[] out = new String[outEvents.length];
		for (int i=0;i<out.length;i++)
			out[i]=outEvents[i].getName();
		return out;
	}
	
	public TimerEvent[] getFiredEvents() {
		if (!running) return new TimerEvent[0];
		Vector<TimerEvent> out = new Vector<TimerEvent>();
		for (int i=0;i<events.size();i++)
			if (events.elementAt(i).isFiring(getCurrentTime())) out.add(events.elementAt(i));
		TimerEvent[] outStrings = new TimerEvent[out.size()];
		for (int i=0;i<out.size();i++) outStrings[i] = out.elementAt(i);
		return outStrings;
	}
	
	public void addEvent(TimerEvent e) {
		events.add(e);
	}
	
	public void addEvent(String eventName, long oneShotTime) {
		addEvent(new TimerEvent(eventName,oneShotTime));
	}
	
	public void addEvent(String eventName, long startTime, long endTime) {
		addEvent(new TimerEvent(eventName,startTime,endTime));
	}
	
	
	
	public static final Timer createStartedTimer(int delay) {
		Timer t = new Timer(delay);
		t.start();
		return t;
	}
	
	public static final Timer createStartedTimer(int delay, boolean looping) {
		Timer t = new Timer(delay,looping);
		t.start();
		return t;
	}

	private void start() {
		startTime = System.currentTimeMillis();
		running = true;
	}
	
	public boolean hasFinished() {
		if (looping) return false;
		return (running && System.currentTimeMillis()>startTime+delay);
	}

	public long getCurrentTime() {
		if (!running) return 0;
		if (hasFinished()) return delay;
		if (looping) {
			while (System.currentTimeMillis()-startTime>delay) {
				startTime+=delay;
				for (int i=0;i<events.size();i++)
					events.elementAt(i).resetFiring();
			}
		}
		return System.currentTimeMillis() - startTime;
	}

	public void addEvents(TimerEvent[] events) {
		if (events==null) return;
		for (int i=0;i<events.length;i++) {
			addEvent(events[i]);
		}
	}
	
}
