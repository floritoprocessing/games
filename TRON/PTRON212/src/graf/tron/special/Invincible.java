package graf.tron.special;

import graf.pimage.GImage;
import graf.tron.TronPlayer;
import graf.tron.TronSettings;
import processing.core.PApplet;
import processing.core.PGraphics;

public class Invincible extends SpecialDefault implements Special {

	public Invincible(int x, int y, int type, long activeTime, GImage gameCanvas, PGraphics occupied, TronPlayer[] playerList, int playerId) {
		super(x, y, type, activeTime, gameCanvas, occupied, playerList, playerId);
	}

	public int getScore() {
		return TronSettings.SPECIAL_SCORE_INVINCIBLE;
	}

	public void influence() {
		playerList[playerId].setInvincible(true);
	}

	public boolean isActive() {
		return getRunningTime()<getActiveTime();
	}

	public void stopInfluence() {
		playerList[playerId].setInvincible(false);
	}

	public void update() {
	}
	
	public void draw(PApplet pa) {
		super.draw(pa);
		pa.stroke(255,240,15);
		pa.noFill();
		//float wid = SpecialDefault.TRANSFER_DISTANCE*0.6f;
		float hei = TronSettings.SPECIAL_TRANSFER_DISTANCE*1.2f;
		pa.line(x, y-hei/2, x, y+hei/2);
	}

	public void drawActive(PApplet pa, float x, float y, int color) {
		pa.noFill();
		pa.ellipseMode(PApplet.CENTER);
		for (float p=0;p<1.0;p+=0.2f) {
			int opac = 0 + (int)(255*p);
			pa.stroke(opac<<24|color);
			float rad = 5 + p*5;
			pa.ellipse(x, y, TronSettings.SPECIAL_TRANSFER_DISTANCE+rad, TronSettings.SPECIAL_TRANSFER_DISTANCE+rad);
		}
	}


}
