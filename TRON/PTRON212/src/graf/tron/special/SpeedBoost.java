package graf.tron.special;

import graf.pimage.GImage;
import graf.tron.TronPlayer;
import graf.tron.TronSettings;
import processing.core.PApplet;
import processing.core.PGraphics;

public class SpeedBoost extends SpecialDefault implements Special {

	public SpeedBoost(int x, int y, int type, long activeTime, GImage gameCanvas, PGraphics occupied, TronPlayer[] playerList, int playerId) {
		super(x,y,type,activeTime, gameCanvas, occupied, playerList, playerId);
	}
	
	public int getScore() {
		return TronSettings.SPECIAL_SCORE_SPEED_BOOST;
	}

	public void influence() {
		playerList[playerId].setSpeed(TronSettings.PLAYER_SPEED*3);
	}
	
	public boolean isActive() {
		return getRunningTime()<getActiveTime();
	}

	public void stopInfluence() {
		playerList[playerId].setSpeed(TronSettings.PLAYER_SPEED);
	}

	public void update() {
		
	}

	public void draw(PApplet pa) {
		super.draw(pa);
		pa.stroke(255,240,15);
		pa.noFill();
		float wid = TronSettings.SPECIAL_TRANSFER_DISTANCE*1.2f;
		float hei = TronSettings.SPECIAL_TRANSFER_DISTANCE*0.6f;
		pa.arc(x, y-hei/2, wid, hei, (float)(0.5*Math.PI), (float)(2*Math.PI));
		pa.arc(x, y+hei/2, wid, hei, (float)(1.5*Math.PI), (float)(3*Math.PI));
	}

	public void drawActive(PApplet pa, float x, float y, int color) {
		// TODO Auto-generated method stub
		
	}
	

	

}
