package graf.tron.special;

import processing.core.PApplet;

public interface Special {

	/**
	 * Get special score
	 * @return int
	 */
	public int getScore();
	
	/**
	 * draw and influence on player, canvas, ocucpied etc..
	 *
	 */
	public void influence();
	
	
	/**
	 * Check if special is still active (after shooting)
	 * @return
	 */
	public boolean isActive();
	

	/**
	 * Stop influence
	 *
	 */
	public void stopInfluence();
	
	/**
	 * Timely update, i.e. move, count down, etc..
	 *
	 */
	public void update();

	
	/**
	 * Draw special around player
	 * @param pa 
	 * @param y 
	 * @param x 
	 * @param specialColor 
	 *
	 */
	public void drawActive(PApplet pa, float x, float y, int specialColor);

}
