package graf.tron.special;

import graf.pimage.GImage;
import graf.tron.TronPlayer;
import graf.tron.TronSettings;

import java.util.Vector;

import processing.core.PApplet;
import processing.core.PGraphics;

public class SpecialCollection {

	private final int width;
	private final int height;
	//private final GImage gameCanvas;
	//private final PGraphics occupied;
	private final TronPlayer[] playerList;
	
	private Vector<SpecialDefault> specials = new Vector<SpecialDefault>();
	public SpecialCollection(int width, int height, GImage gameCanvas, PGraphics occupied, TronPlayer[] playerList) {
		this.width=width;
		this.height=height;
		//this.gameCanvas=gameCanvas;
		//this.occupied=occupied;
		this.playerList = playerList;
		for (int i=0;i<playerList.length;i++) {
			generate(gameCanvas, occupied, true);
			//int x = (int)(Math.random()*width);
			//int y = (int)(Math.random()*height);
			//specials.add(SpecialDefault.create(x,y,gameCanvas, occupied, playerList, -1, SpecialDefault.SPECIAL_SURPRISE));
		}
	}
	
	/**
	 * Generates new Specials
	 * @see TronSettings#SPECIAL_CREATE_CHANCE
	 * @see TronSettings#SPECIALS_AMOUNT_MAX
	 * @param gameCanvas
	 * @param occupied
	 */
	public void generate(GImage gameCanvas, PGraphics occupied) {
		generate(gameCanvas, occupied, false);
	}
	
	/**
	 * Generates new Specials
	 * @see TronSettings#SPECIAL_CREATE_CHANCE
	 * @see TronSettings#SPECIALS_AMOUNT_MAX
	 * @param gameCanvas
	 * @param occupied
	 * @param alwaysGenerate generate always or generate depending on settings
	 */
	public void generate(GImage gameCanvas, PGraphics occupied, boolean alwaysGenerate) {
		if (specials.size()<TronSettings.SPECIALS_AMOUNT_MAX) {
			if (Math.random()<TronSettings.SPECIAL_CREATE_CHANCE || alwaysGenerate) {
				boolean tooClose=true;
				while (tooClose) {
					int x = (int)(Math.random()*width);
					int y = (int)(Math.random()*height);
					tooClose = false;
					for (int xo=-(int)TronSettings.SPECIAL_TRANSFER_DISTANCE;xo<=(int)TronSettings.SPECIAL_TRANSFER_DISTANCE;xo++)
						for (int yo=-(int)TronSettings.SPECIAL_TRANSFER_DISTANCE;yo<=(int)TronSettings.SPECIAL_TRANSFER_DISTANCE;yo++) {
							int tx=x+xo;
							int ty=y+yo;
							if (tx<0||tx>=gameCanvas.width||ty<0||ty>=gameCanvas.height) tooClose=true;
							else if (occupied.get(tx,ty)==0xffffffff) tooClose=true;
						}
					 
					if (!tooClose)
						specials.add(SpecialDefault.create(x,y,gameCanvas, occupied, playerList, -1, SpecialDefault.SPECIAL_SURPRISE));
				}
			}
		}
	}
	
	public void transferToPlayers() {
		for (int i=0;i<playerList.length;i++) {
			float xp = playerList[i].getX();
			float yp = playerList[i].getY();
			for (int j=0;j<specials.size();j++) {
				float dx = xp-specials.elementAt(j).getX();
				float dy = yp-specials.elementAt(j).getY();
				if (Math.sqrt(dx*dx+dy*dy)<TronSettings.SPECIAL_TRANSFER_DISTANCE) {
					playerList[i].specialStopCurrent();
					playerList[i].specialSet(specials.elementAt(j));
					specials.removeElementAt(j);
					j--;
				}
			}
		}
	}
	
	public void draw(PApplet pa) {	
		for (int j=0;j<specials.size();j++) {
			specials.elementAt(j).draw(pa);
		}
	}

	
	
	
}
