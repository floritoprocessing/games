package graf.pimage;

import java.awt.Image;
import java.util.Vector;
import processing.core.PApplet;
import processing.core.PImage;

public class GImage extends PImage {

	public GImage() {
		super();
	}
	
	public GImage(int width, int height) {
		super(width,height);
	}
	
	public GImage(Image img) {
		super(img);
	}

	private int mode;
	public static final int MODE_COPY = 0;
	public static final int MODE_ADD = 1;

	public void setBlendMode(int mode)  {
		if (mode!=MODE_COPY && mode!=MODE_ADD) throw new RuntimeException("Blend mode unknown!");
		this.mode = mode;
	}

	public int getBlendMode() {
		return mode;
	}



	public void set(int x, int y, int rgb) {
		if (x<0||x>=width||y<0||y>=height) return;
		else if (mode==MODE_COPY)
			super.set(x,y,rgb);
		else if (mode==MODE_ADD) 
			super.set(x,y,blend(rgb,get(x,y),mode));
	}
	
	public static void set(PApplet pa, int mode, int x, int y, int rgb) {
		if (x<0||x>=pa.width||y<0||y>=pa.height) return;
		else if (mode==MODE_COPY)
			pa.set(x,y,rgb);
		else if (mode==MODE_ADD) 
			pa.set(x,y,blend(rgb,pa.get(x,y),mode));
	}
	

	
	
	public void set(int x, int y, int rgb, float opac) {
		if (x<0||x>=width||y<0||y>=height) return;
		else if (mode==MODE_COPY)
			super.set(x,y,blend(rgb,opac,get(x,y),mode));
		else if (mode==MODE_ADD)
			super.set(x,y,blend(rgb,opac,get(x,y),mode));
	}
	
	public static void set(PApplet pa, int mode, int x, int y, int rgb, float opac) {
		if (x<0||x>=pa.width||y<0||y>=pa.height) return;
		else if (mode==MODE_COPY)
			pa.set(x,y,blend(rgb,opac,pa.get(x,y),mode));
		else if (mode==MODE_ADD)
			pa.set(x,y,blend(rgb,opac,pa.get(x,y),mode));
	}
	
	
	
	

	public void set(float x, float y, int rgb, float opac) {
		if (x<0||x>=width||y<0||y>=height) return;
		int x0=(int)x, x1=x0+1;
		int y0=(int)y, y1=y0+1;
		float px1 = x-x0, px0=1-px1;
		float py1 = y-y0, py0=1-py1;
		float p00 = opac*px0*py0;
		float p10 = opac*px1*py0;
		float p01 = opac*px0*py1;
		float p11 = opac*px1*py1;
		set(x0,y0,rgb,p00);
		set(x1,y0,rgb,p10);
		set(x0,y1,rgb,p01);
		set(x1,y1,rgb,p11);
	}
	
	public static void set(PApplet pa, int mode, float x, float y, int rgb, float opac) {
		if (x<0||x>=pa.width||y<0||y>=pa.height) return;
		int x0=(int)x, x1=x0+1;
		int y0=(int)y, y1=y0+1;
		float px1 = x-x0, px0=1-px1;
		float py1 = y-y0, py0=1-py1;
		float p00 = opac*px0*py0;
		float p10 = opac*px1*py0;
		float p01 = opac*px0*py1;
		float p11 = opac*px1*py1;
		set(pa,mode,x0,y0,rgb,p00);
		set(pa,mode,x1,y0,rgb,p10);
		set(pa,mode,x0,y1,rgb,p01);
		set(pa,mode,x1,y1,rgb,p11);
	}
	
	
	

	public void set(float x, float y, int rgb) {
		set(x,y,rgb,1.0f);
	}
	
	public static void set(PApplet pa, int mode, float x, float y, int rgb) {
		set(pa,mode,x,y,rgb,1.0f);
	}






	public static final int blend(int rgb1, int rgb2, int mode) {
		if (mode==MODE_ADD) {
			return maxcolor(red(rgb1)+red(rgb2), green(rgb1)+green(rgb2), blue(rgb1)+blue(rgb2));
		}
		return 0;
	}

	public static final int blend(int rgb1, float opac1, int rgb2, int mode) {
		if (mode==MODE_COPY) {
			float opac2=1-opac1;
			return maxcolor(opac1*red(rgb1)+opac2*red(rgb2),
					opac1*green(rgb1)+opac2*green(rgb2),
					opac1*blue(rgb1)+opac2*blue(rgb2));
		}
		if (mode==MODE_ADD) {
			return maxcolor(opac1*red(rgb1)+red(rgb2), opac1*green(rgb1)+green(rgb2), opac1*blue(rgb1)+blue(rgb2));
		}
		return 0;
	}

	public static final int maxcolor(float r, float g, float b) {
		return max(r)<<16|max(g)<<8|max(b);
	}

	public static final int maxcolor(int r, int g, int b) {
		return max(r)<<16|max(g)<<8|max(b);
	}

	public static final int color(int r, int g, int b) {
		return r<<16|g<<8|b;
	}

	public static final int max(float v) {
		return (int)(v>255?255:v);
	}

	public static final int red(int rgb) {
		return rgb>>16&0xff;
	}

	public static final int green(int rgb) {
		return rgb>>8&0xFF;
	}

	public static final int blue(int rgb) {
		return rgb&0xFF;
	}

	public static final int LINE_MODE_EXCLUSIVE = 0;
	public static final int LINE_MODE_INCLUSIVE = 1;

	public Vector<float[]> lineRGB(int x0, int y0, int x1, int y1, int rgb, int lineMode) {
		return lineRGB(x0, y0, x1, y1, rgb, 1, lineMode);
	}
	
	public Vector<float[]> lineRGB(int x0, int y0, int x1, int y1, int rgb, float opacity, int lineMode) {
		if (lineMode!=LINE_MODE_EXCLUSIVE&&lineMode!=LINE_MODE_INCLUSIVE) throw new RuntimeException("Unknown line mode");

		double dx = x1-x0, dy=y1-y0;
		Vector<float[]> out = new Vector<float[]>();
		
		if (x0==x1 && y0==y1) {
			set(x0,y0,rgb,opacity);
			out.add(new float[] {x0,y0});
			return out;
		}

		if (x0==x1) {
			double d = Math.abs(y1-y0) + lineMode;
			for (float fd=0;fd<d;fd+=1) {
				double p = fd/d;
				int y = (int)(y0+dy*p);
				set(x0, y, rgb, opacity);
				out.add(new float[] {x0,y});
			}
			return out;
		}

		else if (y0==y1) {
			double d = Math.abs(x1-x0) + lineMode;
			for (float fd=0;fd<d;fd+=1) {
				double p = fd/d;
				int x = (int)(x0+dx*p);
				set(x, y0, rgb, opacity);
				out.add(new float[] {x,y0});
			}
			return out;
		}

		else {
			double d = Math.sqrt(dx*dx+dy*dy) + lineMode;		
			for (float fd=0;fd<d;fd+=1) {
				double p = fd/d;
				float x=(float)(x0+dx*p),y=(float)(y0+dy*p); 
				set(x, y, rgb, opacity);
				out.add(new float[] {x,y});
			}
			return out;
		}

	}

	public static void line(PApplet pa, int mode, float x0, float y0, float x1, float y1, int color) {
		float dx=x1-x0;
		float dy=y1-y0;
		double d = Math.sqrt(dx*dx+dy*dy);
		for (double dd=0;dd<d;dd++) {
			double p = dd/d;
			double x = x0+p*dx;
			double y = y0+p*dy;
			set(pa,mode,(float)x,(float)y,color);
		}
	}

	public static void addImageOnApplet(GImage gameCanvas, PApplet pa) {//, float opacity) {
		if (gameCanvas.width!=pa.width) return;
		if (gameCanvas.height!=pa.height) return;
		for (int x=0;x<pa.width;x++) for (int y=0;y<pa.height;y++) {
			int col = gameCanvas.get(x,y);
			if ((col&0xffffff)!=0)
				set(pa,MODE_ADD,x,y,col);//,opacity);
		}
	}
	
	public void decolorize(float speed, int minSaturation) {
		setBlendMode(MODE_COPY);
		float invSpeed =1-speed;
		for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
			int col = get(x,y);
			int rgb = col&0xffffff;
			if (rgb!=0) {
				int r = red(rgb);
				int g = green(rgb);
				int b = blue(rgb);
				int saturation = (Math.abs(r-g)+Math.abs(g-b)+Math.abs(r-b))/3;
				if (saturation>minSaturation) {
					float avCol = (r+g+b)/3.0f;
					float avColSpeed = avCol*speed;
					r = (int)(avColSpeed+invSpeed*r);
					g = (int)(avColSpeed+invSpeed*g);
					b = (int)(avColSpeed+invSpeed*b);
					set(x,y,(col&0xff000000)|r<<16|g<<8|b);
				}
			}
		}
	}

	public void clear(int color) {
		for (int x=0;x<width;x++) for (int y=0;y<height;y++) set(x,y,color);
	}



}
