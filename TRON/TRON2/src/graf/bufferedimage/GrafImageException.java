package graf.bufferedimage;

public class GrafImageException extends Exception {
	
	private static final long serialVersionUID = -1089657535955430238L;

	public GrafImageException(String msg) {
		super(msg);
	}

}
