package graf.tron;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;

/*
 * Here is how in Win XP: 
 * 1. Setting -> Control Panel -> Accessibility Options 
 * 2. Filter Keys check the 'Use FilterKeys' 
 * 3. Click Settings button 
 * 4. Filter options select 'Ignore quick keystrokes and slow down repeat rate' 
 * 5. Click settings 
 * 6. Select 'No keyboard repeat' 
 * 7. Click 'OK' 
 * 8. Notification Uncheck 'Beep when keys pressed or accepted' 
 * 9. Click 'OK' 
 * 10. Click 'Apply' 
 * 11. Click 'OK' 
 * Now you can hold a key down and have the keyDown event trigger and navigate 
 * while holding it down and then the keyUp event will trigger when you let go of the key
 */

public class Tron extends JFrame implements ActionListener, KeyListener, WindowListener {
	
	private static final long serialVersionUID = 1L;
	
	private TronTask tronTask;
	private final TronCanvas tronCanvas;
	private final TronOccupiedCanvas tronOccupiedCanvas;
	private TronGame tronGame;
	
	private final JButton newGameButton = new JButton("New Game");
	private final JButton endGameButton = new JButton("End Game");
	private final JToggleButton occupiedVisibleButton = new JToggleButton("Occupied visible");
	private final JLabel fpsLabel = new JLabel("... fps");
	
	private static final int WIDTH = 800;
	private static final int HEIGHT = 300;
		
	public Tron() {
		super("Tron");
		setLayout(new GridBagLayout());
		addWindowListener(this);
		setFocusable(true);
		
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(5,10,5,10);
		c.gridx=0;
		c.gridy=0;
		newGameButton.addActionListener(this);
		getContentPane().add(newGameButton,c);
		c.gridx=1;
		endGameButton.addActionListener(this);
		endGameButton.setEnabled(false);
		getContentPane().add(endGameButton,c);
		c.gridx=2;
		occupiedVisibleButton.addActionListener(this);
		getContentPane().add(occupiedVisibleButton,c);
		c.gridx=3;
		c.anchor = GridBagConstraints.EAST;
		getContentPane().add(fpsLabel,c);
		c.anchor = GridBagConstraints.WEST;
		c.gridx=0;
		c.gridy=1;
		c.gridwidth=4;
		tronOccupiedCanvas = new TronOccupiedCanvas(WIDTH,HEIGHT);
		tronOccupiedCanvas.setVisible(false);
		tronCanvas = new TronCanvas(WIDTH,HEIGHT,tronOccupiedCanvas);
		tronCanvas.addKeyListener(this);
		getContentPane().add(tronCanvas,c);
		c.gridy=2;
		getContentPane().add(tronOccupiedCanvas,c);
		pack();
		setResizable(false);
		setVisible(true);
	}
	
	public void displayFps(String msg) {
		fpsLabel.setText(msg);
	}
	
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource().equals(newGameButton)) {
			newGameButton.setEnabled(false);
			endGameButton.setEnabled(true);
			tronGame = new TronGame(WIDTH,HEIGHT);
			tronCanvas.requestFocus();
			tronCanvas.clear();
			(tronTask = new TronTask(this,tronCanvas,tronOccupiedCanvas,tronGame)).execute();
		}
		else if (arg0.getSource().equals(endGameButton)) {
			newGameButton.setEnabled(true);
			endGameButton.setEnabled(false);
			tronTask.cancel(true);
			tronTask = null;
			tronGame = null;
		}
		else if (arg0.getSource().equals(occupiedVisibleButton)) {
			tronOccupiedCanvas.setVisible(occupiedVisibleButton.isSelected());
			pack();
		}
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	new Tron();
            }
		});
	}

	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowClosing(WindowEvent arg0) {
		System.exit(0);
	}

	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void keyPressed(KeyEvent arg0) {
		if (tronGame!=null) {
			tronGame.keyPressed(arg0);
		}
	}

	public void keyReleased(KeyEvent arg0) {
		if (tronGame!=null) {
			tronGame.keyReleased(arg0);
		}
	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
	
}
