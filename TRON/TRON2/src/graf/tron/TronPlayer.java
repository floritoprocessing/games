package graf.tron;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class TronPlayer {

	private final int id;
	private final String name;
	private final int color;
	
	private BufferedImage[] image = new BufferedImage[4];
	private BufferedImage[][] aura = new BufferedImage[4][10];
	private final float imageScale = 1;
	
	private float x, y;
	private float lastX, lastY;
	private float movX, movY;
	private int direction;
	private float speed = 5f; // pixel per frame
	
	private boolean alive = true;

	private long specialStartTime;
	private static final long specialWaitTime = 1000;
	private boolean specialCharging = false;
	private float specialChargePercentage = 0;
	
	public TronPlayer(TronGame game, int id, String name, int color, float x, float y) {
		this.id = id;
		this.x=x;
		this.y=y;
		lastX = x;
		lastY = y;
		if (Math.random()<0.5) {
			movX=0;
			movY=(y<game.getHeight()/2?speed:-speed);
		} else {
			movX=(x<game.getWidth()/2?speed:-speed);
			movY=0;
		}
		setDirectionFromMovXY();
		this.name = name;
		this.color = color;
		
		BufferedImage[] auraBasic = new BufferedImage[4];
		
		try {
			image[0] = ImageIO.read(new File("./"+TronPlayerSettings.imgName[id]));
			createDirectionalCopies(image);
			auraBasic[0] = ImageIO.read(new File("./"+TronPlayerSettings.auraName[id]));
			createDirectionalCopies(auraBasic);
			for (int i=0;i<4;i++)
				createAlphaCopies(auraBasic[i],aura[i]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void createAlphaCopies(BufferedImage src, BufferedImage[] dest) {
		for (int i=0;i<dest.length;i++) {
			dest[i] = new BufferedImage(src.getWidth(),src.getHeight(),BufferedImage.TYPE_INT_ARGB);
			int[] srcPixels = src.getRGB(0,0,src.getWidth(),src.getHeight(),null,0,src.getWidth());
			float perc = (float)i/(dest.length-1);
			for (int j=0;j<srcPixels.length;j++) {
				int a = (int)(perc*(srcPixels[j]>>24&0xff));
				int rgb = srcPixels[j]&0xffffff;
				srcPixels[j] = a<<24|rgb;
			}
			dest[i].setRGB(0,0,src.getWidth(),src.getHeight(),srcPixels,0,src.getWidth());
		}
	}

	private void createDirectionalCopies(BufferedImage[] image) {
		image[1] = new BufferedImage(image[0].getHeight(),image[0].getWidth(),BufferedImage.TYPE_INT_ARGB);
		image[2] = new BufferedImage(image[0].getWidth(),image[0].getHeight(),BufferedImage.TYPE_INT_ARGB);
		image[3] = new BufferedImage(image[0].getHeight(),image[0].getWidth(),BufferedImage.TYPE_INT_ARGB);
		for (int ix=0;ix<image[0].getWidth();ix++) for (int iy=0;iy<image[0].getHeight();iy++)
			image[1].setRGB(image[1].getWidth()-1-iy, ix, image[0].getRGB(ix,iy));
		for (int ix=0;ix<image[0].getWidth();ix++) for (int iy=0;iy<image[0].getHeight();iy++)
			image[2].setRGB(image[2].getWidth()-1-ix,image[2].getHeight()-1-iy,image[0].getRGB(ix,iy));
		for (int ix=0;ix<image[1].getWidth();ix++) for (int iy=0;iy<image[1].getHeight();iy++)
			image[3].setRGB(image[3].getWidth()-1-ix,image[3].getHeight()-1-iy,image[1].getRGB(ix,iy));
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public float getMovX() {
		return movX;
	}
	
	public float getMovY() {
		return movY;
	}
	
	public String getName() {
		return name;
	}

	public void move() {
		if (!alive) return;
		lastX = x;
		lastY = y;
		x += movX;
		y += movY;
	}

	public void left() {
		if (!alive) return;
		if (movX==speed) {
			movX=0;
			movY=-speed;
		}
		else if (movY==-speed) {
			movY=0;
			movX=-speed;
		}
		else if (movX==-speed) {
			movX=0;
			movY=speed;
		}
		else if (movY==speed) {
			movY=0;
			movX=speed;
		}
		setDirectionFromMovXY();
	}

	public void right() {
		if (!alive) return;
		if (movX==speed) {
			movX=0;
			movY=speed;
		}
		else if (movY==speed) {
			movY=0;
			movX=-speed;
		}
		else if (movX==-speed) {
			movX=0;
			movY=-speed;
		}
		else if (movY==-speed) {
			movY=0;
			movX=speed;
		}
		setDirectionFromMovXY();
	}

	private void setDirectionFromMovXY() {
		direction = movY<0?0:(movX>0?1:(movY>0?2:3));
	}
	
	public void draw(TronCanvas tronCanvas) {
		if (!alive) return;
		tronCanvas.tronLine((int)x,(int)y,(int)lastX,(int)lastY,color);
		tronCanvas.drawTronImage((int)x,(int)y,image[direction],imageScale);
		if (specialCharging) tronCanvas.drawSpecial((int)x,(int)y,aura[direction],imageScale,specialChargePercentage);//,color);
	}

	public void kill() {
		alive  = false;
	}

	public void initSpecial() {
		specialStartTime = System.currentTimeMillis();
		specialCharging = true;
		specialChargePercentage = 0;
	}

	/**
	 * Continues counting down to firing special
	 * @return true when still counting, false when fired
	 */
	public boolean continueSpecial() {
		long current = System.currentTimeMillis();
		if (specialStartTime+specialWaitTime>current) {
			specialChargePercentage = (current-specialStartTime)/(float)specialWaitTime;
			specialCharging = true;
			return true;
		} else {
			specialCharging = false;
			doSpecial();
			return false;
		}
	}
	
	public void interruptSpecial() {
		specialCharging = false;
	}
	
	private void doSpecial() {
		System.out.println("SPECIAL FIRED!");
		specialCharging = false;
	}



}
