package graf.tron;

import java.awt.event.KeyEvent;

public class TronPlayerSettings {
	
	public static final float SPLAT_SIZE = 2;
	public static final float SPLAT_GROW_SPEED  = 1.0f; // per frame
	public static final float SPLAT_OPACITY = 0.3f;
	
	public static final int SPECIAL_RADIUS = 20;
	
	public static final String[] imgName = {
		"TronCar8_red.png",
		"TronCar8_blue.png",
		"TronCar8_green.png"
	};
	
	public static final String[] auraName = {
		"TronAura8_red.png",
		"TronAura8_blue.png",
		"TronAura8_green.png"
	};
	
	// r b g
	public static final int[] color = {
		0xf0200e,
		0x020ef0,
		0x0ef002
	};
	
	public static final int[][] steeringKey = {
		{ KeyEvent.VK_A, KeyEvent.VK_S },
		{ KeyEvent.VK_D, KeyEvent.VK_F },
		{ KeyEvent.VK_G, KeyEvent.VK_H }
	};
}
