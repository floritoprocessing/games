package graf.tron;

import java.awt.event.KeyEvent;
import java.util.Vector;

public class TronGame {

	private int width, height;
	private TronPlayer[] tronPlayer;
	private boolean introFinished = false;
	private boolean gameFinished = false;
	
	private boolean[] left, right, both;

	private static final float INTRO_TIME = 2000;
	private static final float INTRO_GRID_TIME = 1000;
	private static final float INTRO_PREPARE_TIME = 1000;
	private float introTime = INTRO_TIME; // ms

	public TronGame(int width, int height) {
		this.width = width;
		this.height = height;
		tronPlayer = new TronPlayer[2];
		left = new boolean[tronPlayer.length];
		right = new boolean[tronPlayer.length];
		both = new boolean[tronPlayer.length];

		double minDistance = Math.sqrt(width*width+height*height)/4.0;
		boolean tooClose = true;

		while (tooClose) {
			for (int i=0;i<tronPlayer.length;i++) {
				float x = (float)(width*(0.1+0.8*Math.random()));
				float y = (float)(height*(0.1+0.8*Math.random()));
				tronPlayer[i] = new TronPlayer(this,i,"Player "+(i+1),TronPlayerSettings.color[i],x,y);
			}
			
			tooClose=false;
			for (int i=0;i<tronPlayer.length-1;i++) {
				for (int j=i+1;j<tronPlayer.length;j++) {
					float dx = tronPlayer[i].getX()-tronPlayer[j].getX();
					float dy = tronPlayer[i].getY()-tronPlayer[j].getY();
					double distance = Math.sqrt(dx*dx+dy*dy);
					if (distance<minDistance) tooClose=true;
				}
			}
		}

	}

	public void intro(TronCanvas tronCanvas, double frameTimeMs) {
		introTime-=frameTimeMs;
		if (introTime<0) {
			introTime=0;
			introFinished=true;
		}

		float negIntroPercentage = (float)(introTime-INTRO_PREPARE_TIME)/INTRO_GRID_TIME;
		if (negIntroPercentage<0) negIntroPercentage=0;		
		TronBackground.draw(tronCanvas,negIntroPercentage);
	}

	public void move() {
		for (int i=0;i<tronPlayer.length;i++) {
			if (left[i]&&right[i]&&!both[i]) {
				tronPlayer[i].initSpecial();
				both[i]=true;
			} else if (left[i]&&right[i]&&both[i]) {
				both[i] = tronPlayer[i].continueSpecial();
				if (!both[i]) {
					left[i]=false;
					right[i]=false;
				}
			} else if (left[i]) {
				tronPlayer[i].left();
				left[i]=false;
			} else if (right[i]) {
				tronPlayer[i].right();
				right[i]=false;
			} else if (both[i]==false) {
				tronPlayer[i].interruptSpecial();
			}
		}
		for (int i=0;i<tronPlayer.length;i++) 
			tronPlayer[i].move();
	}

	public void draw(TronCanvas tronCanvas) {
		for (int i=0;i<tronPlayer.length;i++) 
			tronPlayer[i].draw(tronCanvas);
	}

	public void keyPressed(KeyEvent arg0) {
		int kc = arg0.getKeyCode();
		for (int i=0;i<tronPlayer.length;i++) {
			if (kc==TronPlayerSettings.steeringKey[i][0]) left[i]=true;
			else if (kc==TronPlayerSettings.steeringKey[i][1]) right[i]=true;
		}
	}
	
	public void keyReleased(KeyEvent arg0) {
		int kc = arg0.getKeyCode();
		for (int i=0;i<tronPlayer.length;i++) {
			if (kc==TronPlayerSettings.steeringKey[i][0]) {
				left[i]=false;
				both[i]=false;
			}
			else if (kc==TronPlayerSettings.steeringKey[i][1]) {
				right[i]=false;
				both[i]=false;
			}
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public boolean introFinished() {
		return introFinished ;
	}

	public boolean finished() {
		return gameFinished;
	}

	public Vector<TronPlayer> checkAndGetCollidedPlayers(TronOccupiedCanvas occupied) {
		
		Vector<TronPlayer> out = new Vector<TronPlayer>();
		
		for (int i=0;i<tronPlayer.length;i++) {
			float x = tronPlayer[i].getX();
			float y = tronPlayer[i].getY();
			int width = occupied.getWidth();
			int height = occupied.getHeight();
			if (x<0||x>=width||y<0||y>=height) {
				tronPlayer[i].kill();
				out.add(tronPlayer[i]);
			}
		}
		
		if (out.size()==tronPlayer.length) gameFinished = true;
		return out;
	}




}
