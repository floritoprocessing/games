package graf.tron;

public class TronStats {

	private double frameTimeMs=0;
	
	public TronStats(double frameTimeMs) {
		this.frameTimeMs = frameTimeMs;
	}

	public double getFrameTimeMs() {
		return frameTimeMs;
	}

}
