package graf.tron;

import graf.bufferedimage.BlendModeException;
import graf.bufferedimage.GrafImage;
import graf.bufferedimage.GrafImageException;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Vector;

public class TronCanvas extends Canvas {

	private static final long serialVersionUID = 5872519959228887067L;

	private GrafImage background;
	private Graphics backgroundG;
	
	private GrafImage backbuffer;
	private Graphics backg;
	
	private final int width, height;
	
	private TronOccupiedCanvas tronOccupiedCanvas;
	private BufferedImage occupied;
	private Graphics occupiedGraphics;

	private Vector<Splat> splats;
	private Vector<Special> specials;
	
	private Vector<Racer> racers;
	
	
	private class Splat {
		
		private final float x, y, maxSize, growSpeed;
		private final int color;
		private final float opacity;
		private float size=0;

		public Splat(float x, float y, float maxSize, float growSpeed, int color, float opacity) {
			this.x=x;
			this.y=y;
			this.maxSize=maxSize;
			this.growSpeed=growSpeed;
			this.color = color;//randomColor(color);
			this.opacity = opacity;
		}
		
		/*private int randomColor(int color) {
			int r = color>>16&0xff;
			int g = color>>8&0xff;
			int b = color&0xff;
			r = (int)Math.max(0,Math.min(255, r + (Math.random()-0.5)*64));
			g = (int)Math.max(0,Math.min(255, g + (Math.random()-0.5)*64));
			b = (int)Math.max(0,Math.min(255, b + (Math.random()-0.5)*64));
			return 0xff<<24|r<<16|g<<8|b;
		}*/

		public boolean growToMax() {
			size += growSpeed;
			if (size>maxSize) return true;
			return false;
		}

		public void draw(GrafImage img, BufferedImage occupied) {
			for (float xo=-size;xo<=size;xo++) for (float yo=-size;yo<=size;yo++) {
				img.setRGB(x+xo, y+yo, color, opacity);
				int sx=(int)(x+xo), sy=(int)(y+yo);
				if (sx>=0&&sx<occupied.getWidth()&&sy>=0&&sy<occupied.getHeight())
					occupied.setRGB(sx, sy, color);
			}
		}

	}
	
	private class Special {
		
		private final int x;
		private final int y;
		private final BufferedImage[] images;
		private final float scale;
		private final float percentage;
		
		public Special(int x, int y, BufferedImage[] images, float scale, float percentage) {
			this.x=x;
			this.y=y;
			this.images=images;
			this.scale=scale;
			this.percentage = percentage;
		}

		public void draw(Graphics g) {
			int i = (int)(images.length*percentage);
			int w = (int)(images[i].getWidth()*scale);//*(1+2*(1-percentage)));
			int h = (int)(images[i].getHeight()*scale);//*(1+2*(1-percentage)));
			int sx = x - w/2;
			int sy = y - h/2;
			g.drawImage(images[i],sx,sy,w,h,null);
		}
	}
	
	private class Racer {
		private int x,y;
		private BufferedImage img;
		private float scale;
		public Racer(int x, int y, BufferedImage img, float scale) {
			this.x=x-(int)(img.getWidth()/2.0*scale);
			this.y=y-(int)(img.getHeight()/2.0*scale);
			this.img=img;
			this.scale=scale;
		}
		public void draw(Graphics g) {
			g.drawImage(img,x,y,(int)(img.getWidth()*scale),(int)(img.getHeight()*scale),null);
		}
	}
	
	public TronCanvas(int width, int height, TronOccupiedCanvas tronOccupiedCanvas) {
		this.width = width;
		this.height = height;
		setPreferredSize(new Dimension(width,height));
		try {
			backbuffer = new GrafImage(width,height,BufferedImage.TYPE_INT_RGB);
			background = new GrafImage(width,height,BufferedImage.TYPE_INT_RGB);
		} catch (GrafImageException e) {
			System.exit(0);
		}
		backg = backbuffer.getGraphics();
		backgroundG = background.getGraphics();
		splats = new Vector<Splat>();
		specials = new Vector<Special>();
		racers = new Vector<Racer>();
		
		this.tronOccupiedCanvas = tronOccupiedCanvas;
		occupied = tronOccupiedCanvas.getBackbufferImage();
		occupiedGraphics = occupied.getGraphics();
	}

	public void clear() {
		backg.setColor(Color.black);
		backg.fillRect(0, 0, width, height);
		backgroundG.setColor(Color.black);
		backgroundG.fillRect(0, 0, width, height);
		occupiedGraphics.setColor(Color.black);
		occupiedGraphics.fillRect(0, 0, width, height);
		splats = new Vector<Splat>();
		repaint();
	}

	public void setBackgroundBlendMode(int mode) {
		try {
			background.setBlendMode(mode);
		} catch (BlendModeException e) {
			e.printStackTrace();
		}
	}

	public Graphics getBackgroundGraphics() {
		return backgroundG;
	}

	public GrafImage getBackgroundImage() {
		return background;
	}
	
	public BufferedImage getOccupied() {
		return occupied;
	}

	public void update(Graphics g) {
		//backg.setColor(Color.black);
		//backg.fillRect(0, 0, backbuffer.getWidth(), backbuffer.getHeight());
		backbuffer.setRGB(0,0,width,height,background.getRGB(0,0,width,height,null,0,width),0,width);
		
		
		if (racers.size()>0) {
			for (int i=0;i<racers.size();i++)
				racers.elementAt(i).draw(backg);
			racers.clear();
		}
		if (specials.size()>0) {
			for (int i=0;i<specials.size();i++)
				specials.elementAt(i).draw(backg);
			specials.clear();
		}
		g.drawImage(backbuffer,0,0,null);
	}

	public void paint(Graphics g) {
		update(g);
	}

	public void tronLine(int x0, int y0, int x1, int y1, int rgb) {
		Vector<float[]> linedots = background.lineRGB(x0, y0, x1, y1, rgb, 1.0f, GrafImage.LINE_MODE_EXCLUSIVE);
		for (int i=0;i<linedots.size();i++) {
			float x = linedots.elementAt(i)[0];
			float y = linedots.elementAt(i)[1];
			if (x>=0&&x<width&&y>=0&&y<height)
				occupied.setRGB((int)x,(int)y, rgb);
			splats.add(new Splat(x,y,
					TronPlayerSettings.SPLAT_SIZE,TronPlayerSettings.SPLAT_GROW_SPEED,rgb,TronPlayerSettings.SPLAT_OPACITY));
		}
	}

	public void repaintIt(boolean repaintOccupied) {
		if (splats!=null) {
			for (int i=0;i<splats.size();i++) {
				if (splats.elementAt(i).growToMax()) {
					splats.remove(splats.elementAt(i));
					i--;
				} else {
					splats.elementAt(i).draw(background,occupied);
				}
			}		
		}
		if (repaintOccupied) tronOccupiedCanvas.repaint();
		this.repaint();
	}

	public void drawSpecial(int x, int y, BufferedImage[] images, float scale, float specialChargePercentage) {
		specials.add(new Special(x,y,images,scale,specialChargePercentage));
	}

	public void drawTronImage(int x, int y, BufferedImage image, float scale) {
		racers.add(new Racer(x,y,image,scale));
	}



}
