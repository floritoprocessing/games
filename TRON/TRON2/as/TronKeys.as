﻿class TronKeys {
	//private var keys:Array = new Array('a','s','d','f','g','h');
	/*
	A - 65
	B - 66
	C - 67
	D - 68
	E - 69
	F - 70
	G - 71
	H - 72
	I - 73
	J - 74
	K - 75
	L - 76
	M - 77
	N - 78
	O - 79
	P - 80
	Q - 81
	R - 82
	S - 83
	T - 84
	U - 85
	V - 86
	W - 87
	X - 88
	Y - 89
	Z - 90
	*/
	private static var keyCodes:Array = new Array(65,83,68,70,71,72);
	
	function TronKeys() {
	}
	
	static function getLeftKey(playerID:Number):Number {
		return keyCodes[playerID*2];
	}
	
	static function getRightKey(playerID:Number):Number {
		return keyCodes[playerID*2+1];
	}
	
	
}
