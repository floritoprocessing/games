﻿class TronPlayer {
	
	public static var MAX_PLAYERS:Number = 3;
	public static var TURNSPEED:Number = 30;
	
	private var id:Number;
	private var canvas:MovieClip;
	private var mc:MovieClip;
	private var width:Number;
	private var height:Number;
	private var active:Boolean = true;
	
	private var speed=5;
	private var rotationSpeed=0;

	private var leftKeyDown:Boolean = false, rightKeyDown:Boolean = false;
	private var bothKeysDown:Boolean = false;

	private static var CHARGE_TIME:Number = 1000;
	private var chargeStartTime:Number;
	private var charging:Boolean = false;
	
	function TronPlayer(id:Number, canvas:MovieClip, mc:MovieClip, width:Number, height:Number) {
		this.id = id;
		this.canvas = canvas;
		this.mc = mc;
		this.width = width;
		this.height = height;
		mc.linkToScript=this;
		mc.onEnterFrame = function() {
			this.linkToScript.update();
		}
	}
	
	function update() {
		var lastX = mc._x;
		var lastY = mc._y;
		mc._y -= speed * Math.cos(mc._rotation*Math.PI/180);
		mc._x += speed * Math.sin(mc._rotation*Math.PI/180);
		
		canvas.lineStyle(1,0xffffff);
		canvas.moveTo(lastX,lastY);
		canvas.lineTo(mc._x,mc._y);
		
		
		mc._rotation += rotationSpeed;
		if (mc._rotation%90==0) rotationSpeed=0;
		
		if (charging) {
			var percentage = ((new Date()).getTime() - chargeStartTime)/CHARGE_TIME;
			mc.aura._alpha = Math.pow(percentage,2) * 100;
			if (percentage>1) {
				charging = false;
				mc.aura._alpha = 0;
				shoot();
			}
		}
	}
	
	function setActive(b:Boolean) {
		active = b;
	}
	
	function onKeyDown() {
		//if (Key.isDown(TronKeys.getLeftKey(id)) && Key.isDown(TronKeys.getRightKey(id))) {
		//	trace("BOTH");
		//}
		//if (Key.isDown(TronKeys.getLeftKey(id))) turnLeft();
		//else if (Key.isDown(TronKeys.getRightKey(id))) turnRight();
	}
	
	function onKeyUp() {
		switch (Key.getCode()) {
			case TronKeys.getLeftKey(id): leftKeyDown=false; bothKeysDown=false; break;
			case TronKeys.getRightKey(id): rightKeyDown=false; bothKeysDown=false; break;
			default: break;
		}
	}
	
	function shoot() {
		trace("shoot");
	}
	
	function startCharge() {
		charging = true;
		chargeStartTime = (new Date()).getTime();
	}
	
	function bothKeys() {
		leftKeyDown=false;
		rightKeyDown=false;
		if (bothKeysDown==false) {
			startCharge();
			bothKeysDown=true;
		}
	}
	
	function leftKey() {
		if (leftKeyDown==false) {
			leftKeyDown=true;
			turnLeft();
		}
	}
	
	function rightKey() {
		if (rightKeyDown==false) {
			rightKeyDown=true;
			turnRight();
		}
	}
	
	function turnLeft() {
		trace(id+" left");
		rotationSpeed=-TURNSPEED;
	}
	
	function turnRight() {
		trace(id+" right");
		rotationSpeed=TURNSPEED;
	}
}
