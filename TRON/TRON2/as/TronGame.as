﻿import flash.display.BitmapData;
//import TronKeys;

class TronGame {

	var tronPlayer:Array;
	
	var canvas:MovieClip;
	
	var carLevel:Number = 100;
	var mcCar:Array = new Array(3);
	var carBmpName:Array = new Array("CarRed","CarBlue","CarGreen");
	var carAuraName:Array = new Array("AuraRed","AuraBlue","AuraGreen");
		
	var tronKeys:TronKeys;
	
	function TronGame(main:MovieClip, wid:Number, hei:Number) {
		
		//Key.addListener(this);
		
		/*
		*	INIT CANVAS
		*/
		
		canvas = main.createEmptyMovieClip("canvas",0);
		
		/*
		*	INIT CAR GRAPHICS / MOVIECLIPS
		*/
		for (var i=0;i<mcCar.length;i++) {
			mcCar[i] = main.createEmptyMovieClip("Car"+i, carLevel++);
			var bmp:BitmapData = BitmapData.loadBitmap(carBmpName[i]);
			mcCar[i].createEmptyMovieClip("bmp",1);
			mcCar[i].bmp._x = -bmp.width/2;
			mcCar[i].bmp._y = -bmp.height/2;
			mcCar[i].bmp.attachBitmap(bmp,0);
			var aura:BitmapData = BitmapData.loadBitmap(carAuraName[i]);
			mcCar[i].createEmptyMovieClip("aura",0);
			mcCar[i].aura._x = -aura.width/2;
			mcCar[i].aura._y = -aura.height/2;
			mcCar[i].aura.attachBitmap(aura,0);
			mcCar[i].aura._alpha = 0;
			mcCar[i]._x = 50 + i*50;
			mcCar[i]._y = 50;
			mcCar[i]._xscale = mcCar[i]._yscale = 50;
		}
		
		/*
		*	INIT TRON PLAYER
		*/
		tronPlayer = new Array(3);
		for (var i=0;i<TronPlayer.MAX_PLAYERS;i++) {
			tronPlayer[i] = new TronPlayer(i,canvas,mcCar[i],wid,hei);
			Key.addListener(tronPlayer[i]);
		}
		
		main.onEnterFrame = function() {
			this.tronGame.update();
		}
		
		
	}
	
	function onKeyDown() {
		/*for (var i=0;i<TronPlayer.MAX_PLAYERS;i++) {
			if (Key.isDown(TronKeys.getLeftKey(i)) && Key.isDown(TronKeys.getRightKey(i))) {
				trace("both "+i);
			} else if (Key.isDown(TronKeys.getLeftKey(i)))
				trace("left "+i);
			else if (Key.isDown(TronKeys.getRightKey(i)))
				trace("right "+i);
		}*/
	}
	
	
	function update() {
		for (var i=0;i<TronPlayer.MAX_PLAYERS;i++) {
			if (Key.isDown(TronKeys.getLeftKey(i)) && Key.isDown(TronKeys.getRightKey(i))) {
				tronPlayer[i].bothKeys();
			} else if (Key.isDown(TronKeys.getLeftKey(i)))
				tronPlayer[i].leftKey();
			else if (Key.isDown(TronKeys.getRightKey(i)))
				tronPlayer[i].rightKey();
		}
		//trace(TronKeys.getLeftKey(0));
		//if (Key.isDown(TronKeys.getLeftKey(0)) && Key.isDown(TronKeys.getRightKey(0)))
		// trace("both0");
		//if (Key.isDown(tronKeys.getLeftKey(0))) trace("left p1");
		//if (Key.isDown(65) && Key.isDown(83)) trace("BOTH");
	}
	
}
