package graf.tron;

import graf.pimage.GImage;
import graf.tron.special.Special;
import graf.tron.special.SpecialDefault;
import graf.tron.special.SpecialCollection;
import graf.tron.timer.Timer;

import java.awt.event.KeyEvent;
import java.util.Vector;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;

public class TronGame {

	private int width, height;
	private TronPlayer[] tronPlayer;
	private final GImage gameCanvas;
	private final SpecialCollection specialCollection;
	private PGraphics occupied;
	private boolean gameFinished = false;

	private boolean[] left, right, both;
	
	private Timer endTimer = null;

	public TronGame(PTron ptron, String irenderer, int width, int height) {
		this.width = width;
		this.height = height;
		occupied = ptron.createGraphics(width, height, irenderer);
		occupied.beginDraw();
		occupied.background(0);
		occupied.endDraw();
		gameCanvas=ptron.getGameCanvas();
		tronPlayer = new TronPlayer[2];
		left = new boolean[tronPlayer.length];
		right = new boolean[tronPlayer.length];
		both = new boolean[tronPlayer.length];

		double minDistance = Math.sqrt(width*width+height*height)/4.0;
		boolean tooClose = true;

		specialCollection = new SpecialCollection(width,height,gameCanvas,occupied,tronPlayer);

		while (tooClose) {
			for (int i=0;i<tronPlayer.length;i++) {
				float x = (float)(width*(0.1+0.8*Math.random()));
				float y = (float)(height*(0.1+0.8*Math.random()));
				tronPlayer[i] = new TronPlayer(this,i,"Player "+(i+1),TronPlayerSettings.drawColor[i],TronPlayerSettings.PLAYER_TEXT_COLOR[i],x,y);
				tronPlayer[i].specialSet(
						SpecialDefault.create(0,0,gameCanvas, occupied, tronPlayer, i, SpecialDefault.SPECIAL_SURPRISE)
				);
			}

			tooClose=false;
			for (int i=0;i<tronPlayer.length-1;i++) {
				for (int j=i+1;j<tronPlayer.length;j++) {
					float dx = tronPlayer[i].getX()-tronPlayer[j].getX();
					float dy = tronPlayer[i].getY()-tronPlayer[j].getY();
					double distance = Math.sqrt(dx*dx+dy*dy);
					if (distance<minDistance) tooClose=true;
				}
			}
		}
	}

	public PGraphics getOccupied() {
		return occupied;
	}

	public void move() {
		for (int i=0;i<tronPlayer.length;i++) {
			if (tronPlayer[i].isFrozen()) {
				both[i]=false;
				left[i]=false;
				right[i]=false;
			} else {
				if (left[i]&&right[i]&&!both[i]) {
					tronPlayer[i].specialInit();
					both[i]=true;
				} else if (left[i]&&right[i]&&both[i]) {
					both[i] = tronPlayer[i].specialChargeContinue();
					if (!both[i]) {
						left[i]=false;
						right[i]=false;
					}
				} else if (left[i]) {
					tronPlayer[i].left();
					left[i]=false;
				} else if (right[i]) {
					tronPlayer[i].right();
					right[i]=false;
				} else if (both[i]==false) {
					tronPlayer[i].specialInterrupt();
				}
			}
		}
		for (int i=0;i<tronPlayer.length;i++) 
			tronPlayer[i].move();
	}

	/**
	 * Draws tron player, and checks collision -> kill() before drawing
	 * @param ptron
	 * @param introPercentage
	 */
	public void draw(PTron ptron, float introPercentage) {
		for (int i=0;i<tronPlayer.length;i++) {
			if (!tronPlayer[i].draw(ptron,occupied,introPercentage));
		}
	}

	/**
	 * Draws tron player, and checks collision -> kill() before drawing 
	 * @param tron
	 */
	public void draw(PTron tron) {
		draw(tron,0);
	}

	public void keyPressed(KeyEvent arg0) {
		int kc = arg0.getKeyCode();
		for (int i=0;i<tronPlayer.length;i++) {
			if (kc==TronPlayerSettings.steeringKey[i][0]) left[i]=true;
			if (kc==TronPlayerSettings.steeringKey[i][1]) right[i]=true;
		}
	}

	public void keyReleased(KeyEvent arg0) {
		int kc = arg0.getKeyCode();
		for (int i=0;i<tronPlayer.length;i++) {
			if (kc==TronPlayerSettings.steeringKey[i][0]) {
				left[i]=false;
				both[i]=false;
			}
			else if (kc==TronPlayerSettings.steeringKey[i][1]) {
				right[i]=false;
				both[i]=false;
			}
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public boolean finished() {
		if (!gameFinished) {
			int deadCount=0;
			for (int i=0;i<tronPlayer.length;i++) if (!tronPlayer[i].isAlive()) deadCount++;
			if (deadCount>=tronPlayer.length-1) {
				endTimer = Timer.createStartedTimer(5000);
				gameFinished=true;
			}
		}
		return gameFinished;
	}

	public void getCollidedPlayers() {
		for (int i=0;i<tronPlayer.length;i++) {
			float x0 = tronPlayer[i].getX();
			float y0 = tronPlayer[i].getY();
			// out of bounds:
			if (x0<0||x0>=width||y0<0||y0>=height) {
				if (tronPlayer[i].isInvincible()) {
					// clip
					if (x0<0) tronPlayer[i].jumpX(x0+width);
					else if (x0>=width) tronPlayer[i].jumpX(x0-width);
					else if (y0<0) tronPlayer[i].jumpY(y0+height);
					else if (y0>=height) tronPlayer[i].jumpY(y0-height);
				} else {
					tronPlayer[i].kill();
				}
			}
		}
	}



	public void stop() {
		for (int i=0;i<tronPlayer.length;i++) {
			tronPlayer[i].stop();
		}
	}

	public TronPlayer getWinner() {
		TronPlayer out = null;
		int winCount=0;
		for (int i=0;i<tronPlayer.length;i++) {
			if (tronPlayer[i].isAlive()) {
				out = tronPlayer[i];
				winCount++;
			}
		}
		if (winCount>1) return null;
		else return out;
	}

	/**
	 * <li>Updates all active specials
	 * <li>Generates Specials
	 * <li>Transfer Specials to TronPlayers
	 */
	public void updateSpecials() {


		for (int i=0;i<tronPlayer.length;i++) {
			SpecialDefault special = tronPlayer[i].getSpecial();
			if (special!=null && special.isFired()) {
				((Special)special).update();
				((Special)special).influence();
				if (!((Special)special).isActive()) {
					((Special)special).stopInfluence();
					tronPlayer[i].specialRemove();
				}
			}
		}

		specialCollection.generate(gameCanvas,occupied);

		specialCollection.transferToPlayers();
	}


	/**
	 * Draws the specials on screen
	 * Also draws active specials like invincibility around player
	 * @param pa
	 * @param font
	 */
	public void specialsDraw(PApplet pa) {
		specialCollection.draw(pa);
		for (int i=0;i<tronPlayer.length;i++)
			tronPlayer[i].specialDraw(pa);
	}


	public void drawScore(PApplet pa) {
		int players = tronPlayer.length-1;
		float x0 = 20;
		float x1 = pa.width-140;
		for (int i=0;i<tronPlayer.length;i++) {
			pa.fill(0xff000000|tronPlayer[i].getTextColor());
			pa.textAlign(PApplet.LEFT);
			float x = x0 + (x1-x0)*((float)i/players);
			/*
			 * PLAYER NAME
			 */
			pa.text(tronPlayer[i].getName(),x, 40);

			/*
			 * PLAYER SCORE
			 */
			if (tronPlayer[i].isAlive())
				pa.text(tronPlayer[i].getScore(),x,60);
			else
				pa.text("DEAD ("+tronPlayer[i].getScore()+")",x,60);

			/*
			 * PLAYER SPECIAL
			 */
			if (tronPlayer[i].getSpecial()!=null)
				if (!tronPlayer[i].getSpecial().isFired()) {
					pa.text(tronPlayer[i].getSpecial().getTypeName(),x,80);
				}
				else if (((Special)tronPlayer[i].getSpecial()).isActive()) {
					pa.fill(0xff000000|tronPlayer[i].getBrightTextColor());
					pa.text(tronPlayer[i].getSpecial().getTypeName(),x,80);
				}
		}
	}



	public boolean endTimerHasFinished() {
		return (endTimer!=null && endTimer.hasFinished());
	}

}
