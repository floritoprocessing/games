package graf.tron;

public class TronSettings {
	
	public static final int SCREEN_WIDTH = 800;
	public static final int SCREEN_HEIGHT = 300;

	public static final int BACKGROUND_GRIDSIZE = 30;
	public static final int BACKGROUND_GRIDCOLOR = 0xffeaf2cb;
	public static final float BACKGROUND_GRID_OPACITY = 0.3f;
	
	public static final float INTRO_TIME = 2500;
	public static final float INTRO_GRID_TIME = 1500;
	public static final float INTRO_PREPARE_TIME = 1000;
	
	public static final int INTRO_READY_COLOR = 0xfff0200e;
	public static final int INTRO_GO_COLOR = 0xff0ef002;
	
	public static final float GAME_VISUAL_DECOLORIZE_SPEED = 0.02f;
	public static final int GAME_VISUAL_DECOLORIZE_MIN_SATURATION = 64;
	
	public static final float GAME_END_VISUAL_DECOLORIZE_SPEED = 0.02f;
	public static final int GAME_END_VISUAL_DECOLORIZE_MIN_SATURATION = 12;
	
	public static final int PLAYER_SCORE_MOVE = 10;
	public static final int PLAYER_SCORE_TURN = 50;
	public static final float PLAYER_SPEED = 1.8f;	
	
	public static final float SMEAR_SPEED_VARIATION = 1.0f;
	public static final float SMEAR_SPEED_MIN = 0.2f;
	public static final float SMEAR_AGE_SPEED = 0.1f;
	public static final float SMEAR_BASE_OPACITY = 0.5f;
	
	public static final long SPECIAL_CHARGE_TIME = 400;
	public static final double SPECIAL_CREATE_CHANCE = 0.001;
	public static final int SPECIALS_AMOUNT_MAX = 5;
	
	public static final int SPECIAL_SCORE_SPEED_BOOST = 1000;
	public static final int SPECIAL_SCORE_CHAOS = 500;
	public static final int SPECIAL_SCORE_FREEZE = 500;
	public static final int SPECIAL_SCORE_INVINCIBLE = 500;
	
	public static final float SPECIAL_TRANSFER_DISTANCE=6;
	
	
	
	
	

}
