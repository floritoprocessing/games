package graf.tron;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JToggleButton;

import processing.core.PGraphics;

/*
 * Here is how in Win XP: 
 * 1. Setting -> Control Panel -> Accessibility Options 
 * 2. Filter Keys check the 'Use FilterKeys' 
 * 3. Click Settings button 
 * 4. Filter options select 'Ignore quick keystrokes and slow down repeat rate' 
 * 5. Click settings 
 * 6. Select 'No keyboard repeat' 
 * 7. Click 'OK' 
 * 8. Notification Uncheck 'Beep when keys pressed or accepted' 
 * 9. Click 'OK' 
 * 10. Click 'Apply' 
 * 11. Click 'OK' 
 * Now you can hold a key down and have the keyDown event trigger and navigate 
 * while holding it down and then the keyUp event will trigger when you let go of the key
 */

public class TronControl extends JFrame implements ActionListener, WindowListener {
	
	private static final long serialVersionUID = 1L;
	
	private final JButton newGameButton = new JButton("New Game");
	private final JToggleButton canvasVisibleButton = new JToggleButton("Canvas visible",true);
	private final JLabel fpsLabel = new JLabel("... fps");
	private final TronControlCanvas canvas;
	
	private final PTron ptron;
	private class TronControlCanvas extends Canvas {
		
		private static final long serialVersionUID = -6003164310903766113L;
		private BufferedImage backbuffer;
		private Graphics backg;
		private PGraphics pg;

		public TronControlCanvas(int width, int height, PGraphics pg) {
			super();
			this.pg=pg;
			setPreferredSize(new Dimension(width,height));
			backbuffer = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
			backg = backbuffer.createGraphics();
		}
		
		public void update(Graphics g) {
			backbuffer.setRGB(0, 0, pg.width, pg.height, pg.pixels, 0, pg.width);
			g.drawImage(backbuffer,0,0,null);
		}
		
		public void paint(Graphics g) {
			update(g);
		}
		
	}
	
	public TronControl(PTron ptron, TronGame tronGame) {
		super("Tron");
		this.ptron = ptron;
		canvas = new TronControlCanvas(tronGame.getWidth(),tronGame.getHeight(),tronGame.getOccupied());
		setLayout(new GridBagLayout());
		addWindowListener(this);
		setFocusable(true);
		
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(5,10,5,10);
		c.gridx=0;
		c.gridy=1;
		newGameButton.addActionListener(this);
		getContentPane().add(newGameButton,c);
		c.gridx=1;
		canvasVisibleButton.addActionListener(this);
		getContentPane().add(canvasVisibleButton,c);
		c.gridx=3;
		c.anchor = GridBagConstraints.EAST;
		getContentPane().add(fpsLabel,c);
		c.anchor = GridBagConstraints.WEST;
		c.gridx=0;
		c.gridy=0;
		c.gridwidth=4;
		getContentPane().add(canvas,c);
		pack();
		setResizable(false);
		setLocation(ptron.frame.getLocationOnScreen().x-10,ptron.frame.getLocationOnScreen().y-getHeight()-24);
		setVisible(true);
	}
	
	public void displayFps(String msg) {
		fpsLabel.setText(msg);
	}
	
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource().equals(canvasVisibleButton)) {
			canvas.setVisible(canvasVisibleButton.isSelected());
			pack();
			setLocation(ptron.frame.getLocationOnScreen().x-10,ptron.frame.getLocationOnScreen().y-getHeight()-24);
		}
		else if (arg0.getSource().equals(newGameButton)) {
			newGameButton.setEnabled(false);
			ptron.newGameOnNextFrame();
		}
	}
	
	/*public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	new TronControl();
            }
		});
	}*/

	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowClosing(WindowEvent arg0) {
		System.exit(0);
	}

	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void repaintCanvas() {
		if (canvas.isVisible())
			canvas.repaint();
	}


	
	
}
