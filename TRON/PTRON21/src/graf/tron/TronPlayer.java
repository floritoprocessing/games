package graf.tron;

import graf.pimage.GImage;
import graf.tron.special.Special;
import graf.tron.special.SpecialDefault;

import processing.core.PApplet;
import processing.core.PGraphics;

public class TronPlayer {

	private final int id;
	private final String name;
	private final int color, specialColor, textColor, brightTextColor;
	private float x, y;
	private float lastX, lastY;

	private float movX, movY;
	private float speed = TronSettings.PLAYER_SPEED;

	private SpecialDefault special;
	private int score = 0;
	private boolean alive = true;
	private long specialStartTime;

	private boolean specialCharging = false;
	private float specialChargePercentage = 0;
	private boolean frozen = false;
	private boolean invincible = false;

	public TronPlayer(TronGame game, int id, String name, int color, int textColor, float x, float y) {
		this.id = id;
		this.x=x;
		this.y=y;
		lastX = x;
		lastY = y;
		if (Math.random()<0.5) {
			movX=0;
			movY=(y<game.getHeight()/2?speed:-speed);
		} else {
			movX=(x<game.getWidth()/2?speed:-speed);
			movY=0;
		}
		//setDirectionFromMovXY();
		this.name = name;
		this.color = color;
		specialColor = GImage.blend(color,2,color,GImage.MODE_ADD);
		this.textColor = textColor;
		brightTextColor = GImage.blend(textColor,2,textColor,GImage.MODE_ADD);
	}

	/**
	 * Draws tronPlayer on PTron and on occupied, checks collisionc before drawing and kills if necessary
	 * @param ptron
	 * @param occupied
	 * @param introPercentage 
	 * @return alive state
	 */
	public boolean draw(PTron ptron, PGraphics occupied, float introPercentage) {
		if (!alive) return false;
		if (frozen) return true;
		if (speed==0) return true;

		// || frozen
		if ((x==lastX && y==lastY)) {

			ptron.addSmears(Smear.createSmearDot(x,y,color,0));
			if (introPercentage!=0) {
				float triple = ((introPercentage*TronSettings.INTRO_TIME)%400.0f)/100.0f;
				ptron.addSmears(Smear.createSmearDot(x+triple*TronSettings.PLAYER_SPEED*movX,y+triple*TronSettings.PLAYER_SPEED*movY,color,0));
			}

		} else {

						
			GImage.line((PApplet)ptron,GImage.MODE_ADD,x,y,lastX,lastY,color);

			ptron.addSmears(Smear.createSmearLine(x,y,lastX,lastY,movX,movY,color,specialCharging?specialChargePercentage:0));

			occupied.beginDraw();
			occupied.stroke(0xffffffff);

			if (movX==0) {
				int dy = (int)lastY-(int)y;
				int d = Math.abs(dy);
				for (int dd=0;dd<d;dd++) {
					float p = (float)dd/d;
					int drawX = (int)x;
					int drawY = (int)(y+p*dy);
					if (!invincible && occupied.get(drawX,drawY)==0xffffffff) {
						kill();
						occupied.endDraw();
						return false;
					}
					else occupied.set(drawX, drawY, 0xffffffff);
				}
			} else {
				int dx = (int)lastX-(int)x;
				int d = Math.abs(dx);
				for (int dd=0;dd<d;dd++) {
					float p = (float)dd/d;
					int drawX = (int)(x+p*dx);
					int drawY = (int)y;
					if (!invincible && occupied.get(drawX,drawY)==0xffffffff) {
						kill();
						occupied.endDraw();
						return false;
					}
					else occupied.set(drawX, drawY, 0xffffffff);
				}
			}
			occupied.endDraw();
		}

		return true;
	}

	public int getColor() {
		return color;
	}

	public int getID() {
		return id;
	}

	public float getLastX() {
		return lastX;
	}

	public float getLastY() {
		return lastY;
	}

	public float getMovX() {
		return movX;
	}

	public float getMovY() {
		return movY;
	}

	public String getName() {
		return name;
	}

	public int getScore() {
		return score;
	}

	public SpecialDefault getSpecial() {
		return special;
	}

	public int getTextColor() {
		return textColor;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public boolean isAlive() {
		return alive;
	}

	public void kill() {
		alive  = false;
	}

	public void left() {
		if (!alive) return;
		if (frozen) return;

		score += TronSettings.PLAYER_SCORE_TURN * (int)(speed/TronSettings.PLAYER_SPEED);
		if (movX==speed) {
			movX=0;
			movY=-speed;
		}
		else if (movY==-speed) {
			movY=0;
			movX=-speed;
		}
		else if (movX==-speed) {
			movX=0;
			movY=speed;
		}
		else if (movY==speed) {
			movY=0;
			movX=speed;
		}
		//setDirectionFromMovXY();
	}

	public void move() {
		if (!alive) return;
		if (frozen) return;

		score += TronSettings.PLAYER_SCORE_MOVE * (int)(speed/TronSettings.PLAYER_SPEED);
		lastX = x;
		lastY = y;
		x += movX;
		y += movY;
	}

	public void right() {
		if (!alive) return;
		if (frozen) return;

		score += TronSettings.PLAYER_SCORE_TURN * (int)(speed/TronSettings.PLAYER_SPEED);
		if (movX==speed) {
			movX=0;
			movY=speed;
		}
		else if (movY==speed) {
			movY=0;
			movX=-speed;
		}
		else if (movX==-speed) {
			movX=0;
			movY=-speed;
		}
		else if (movY==-speed) {
			movY=0;
			movX=speed;
		}
		//setDirectionFromMovXY();
	}

	/*private void setDirectionFromMovXY() {
		direction = movY<0?0:(movX>0?1:(movY>0?2:3));
	}*/

	public void setSpeed(float speed) {
		this.speed = speed;
		if (movX<0) movX=-speed;
		else if (movX>0) movX=speed;
		if (movY<0) movY=-speed;
		else if (movY>0) movY=speed;
	}

	/**
	 * Continues counting down to firing special
	 * @return true when still counting, false when fired
	 */
	public boolean specialChargeContinue() {
		if (frozen) return false;
		if (special!=null && !special.isFired()) {
			long current = System.currentTimeMillis();
			if (specialStartTime+TronSettings.SPECIAL_CHARGE_TIME>current) {
				specialChargePercentage = (current-specialStartTime)/(float)TronSettings.SPECIAL_CHARGE_TIME;
				specialCharging = true;
				return true;
			} else {
				specialCharging = false;
				specialFire();
				return false;
			}
		}
		return true;
	}

	private void specialFire() {
		if (frozen) return;
		System.out.println("SPECIAL FIRED: "+special.toString());
		score+=((Special)special).getScore();
		special.fire();
		specialCharging = false;
	}

	public void specialInit() {
		if (frozen) return;
		if (special!=null) {
			specialStartTime = System.currentTimeMillis();
			specialCharging = true;
			specialChargePercentage = 0;
		}
	}

	public void specialInterrupt() {
		if (special!=null)
			specialCharging = false;
	}

	public void specialRemove() {
		special = null;
	}

	/**
	 * Sets a special to this player and sets the playerId of the special
	 * @param special
	 */
	public void specialSet(SpecialDefault special) {
		if (this.special!=null) {
			if (special.isFired())
				((Special)special).stopInfluence();
		}
		
		this.special = special;
		special.setPlayerID(id);
		//System.out.println(special.toString()+" received");
	}

	public void stop() {
		movX=0;
		movY=0;
		lastX=x;
		lastY=y;
	}

	public void freeze() {
		frozen=true;
	}

	public void unFreeze() {
		frozen=false;
	}
	
	public boolean isFrozen() {
		return frozen;
	}

	public void specialStopCurrent() {
		if (special!=null) {
			((Special)special).stopInfluence();
			special=null;
		}
	}

	public void setInvincible(boolean b) {
		invincible = b;
	}

	public boolean isInvincible() {
		return invincible;
	}

	public void jumpX(float x) {
		this.x = x;
		this.lastX = x;
	}
	
	public void jumpY(float y) {
		this.y = y;
		this.lastY = y;
	}

	public int getBrightTextColor() {
		return brightTextColor;
	}

	public void specialDraw(PApplet pa) {
		if (special!=null && special.isFired())
			((Special)special).drawActive(pa,x,y,specialColor);
	}








}
