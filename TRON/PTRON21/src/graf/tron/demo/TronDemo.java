package graf.tron.demo;

import graf.tron.PTron;
import graf.tron.timer.Timer;
import graf.tron.timer.TimerEvent;

import java.awt.event.KeyEvent;

public class TronDemo {

	private final PTron tron;
	private final Timer sequencer;
	
	public static final int FULL_DEMO_TIME = 15000;
	public final TimerEvent[] events;
	
	public TronDemo(PTron tron) {
		this.tron=tron;
		
		events = new TimerEvent[] {
				new TimerEvent(new TronTitleText(tron),0,(long)(FULL_DEMO_TIME*0.6)),
				new TimerEvent(new TronGrid(tron),FULL_DEMO_TIME/2,FULL_DEMO_TIME),
				new TimerEvent(new TronPressPlayText(tron),0,FULL_DEMO_TIME)
		};
		
		sequencer = Timer.createStartedTimer(FULL_DEMO_TIME,true);
		sequencer.addEvents(events);
	}

	public void run() {
		tron.background(0);
		
		
		
		//System.out.print(sequencer.getCurrentTime());
		TimerEvent[] events = sequencer.getFiredEvents();
		for (int i=0;i<events.length;i++) 
			if (events[i].isFiring(sequencer.getCurrentTime())) {
				if (events[i].hasTimerEventListener()) {
					float firePercentage = events[i].getFiringPercentage(sequencer.getCurrentTime());
					events[i].getTimerEventListener().update(firePercentage);
				} else {
					
				}
			}
					
			
		//System.out.println();
	}

	public void keyPressed(KeyEvent arg0) {
		tron.newGameOnNextFrame();
	}

	public void keyReleased(KeyEvent arg0) {		
	}

	
	
}
