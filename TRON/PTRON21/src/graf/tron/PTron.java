package graf.tron;

import graf.pimage.GImage;
import graf.tron.demo.TronDemo;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

import processing.core.PApplet;
import processing.core.PFont;

public class PTron extends PApplet implements KeyListener {

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.tron.PTron"});
	}

	private static final int MODE_DEMO = 0;
	private static final int MODE_GAME = 1;
	private int gameMode = MODE_DEMO;


	private TronDemo tronDemo;
	
	private TronGame tronGame;
	private TronGameIntro tronGameIntro;
	//private TronControl tronControl;
	private GImage gameCanvas;
	private boolean newGame=false;

	public PFont font9, font18, font24, font48, font64, font72;

	private Vector<Smear> smears = new Vector<Smear>();

	public void settings() {
		size(TronSettings.SCREEN_WIDTH,TronSettings.SCREEN_HEIGHT,P3D);
	}
	
	public void setup() {
		
		println("Accesibility options -> no key repeat");
		println("Or use http://sdljava.sourceforge.net/docs/api/sdljava/event/SDLKeyboardEvent.html to avoid keyboard repeats");
		if (gameMode==MODE_GAME) newGameOnNextFrame();
		font9 = loadFont("./Radio_Space-9-unsmooth.vlw"); //-unsmooth
		font18 = loadFont("./Radio_Space-18.vlw");
		font24 = loadFont("./Radio_Space-24.vlw");
		font48 = loadFont("./Radio_Space-48.vlw");
		font64 = loadFont("./Radio_Space-64.vlw");
		font72 = loadFont("./Radio_Space-72.vlw");
	}

	public void newGameOnNextFrame() {
		newGame=true;
	}

	public void quitGame() {
		/*if (tronControl!=null) {
			tronControl.dispose();
			tronControl=null;
		}*/
		//if (tronGame!=null)
			tronGame = null;
		//if (tronGameIntro!=null)
			tronGameIntro = null;
		//if (gameCanvas!=null)
			gameCanvas=null;
	}

	public void keyPressed(KeyEvent arg0) {
		if (tronGame!=null) {
			tronGame.keyPressed(arg0);
		}
		else if (tronDemo!=null) {
			tronDemo.keyPressed(arg0);
		}
	}

	public void keyReleased(KeyEvent arg0) {
		if (tronGame!=null) {
			tronGame.keyReleased(arg0);
		}
		else if (tronDemo!=null) {
			tronDemo.keyReleased(arg0);
		}
	}

	public void keyTyped(KeyEvent arg0) {
		//if (arg0.getKeyChar()=='N') newGame();
		//else if (arg0.getKeyChar()=='Q') quitGame();
	}

	public void addSmears(Vector<Smear> s) {
		smears.addAll(s);
	}

	public void draw() {
		if (newGame) {
			newGame=false;
			gameMode = MODE_GAME;
			quitGame();
			background(0);
			tronDemo=null;
			gameCanvas = new GImage(width,height);
			tronGameIntro = new TronGameIntro(this);
			tronGame = new TronGame(this,P3D,TronSettings.SCREEN_WIDTH,TronSettings.SCREEN_HEIGHT);
			//tronControl = new TronControl(this,tronGame);
		}
		
		if (gameMode==MODE_DEMO) {
			if (tronDemo==null) tronDemo = new TronDemo(this);
			
			tronDemo.run();
		}
		

		if (gameMode==MODE_GAME) {

			//tronGame.run();


			// set background black
			background(0);

			// draw full Grid on black
			TronBackground.drawFullGrid(this);

			// draw (additive) the game Canvas
			GImage.addImageOnApplet(gameCanvas,this);

			// decolorize the game Canvas
			gameCanvas.decolorize(TronSettings.GAME_VISUAL_DECOLORIZE_SPEED,
					TronSettings.GAME_VISUAL_DECOLORIZE_MIN_SATURATION);

			if (!tronGameIntro.introFinished()) {

				textFont(font48);
				float iPerc = tronGameIntro.intro();
				tronGame.draw(this,iPerc);

				if (tronGameIntro.introFinished()) {
					smears.clear();
					gameCanvas.clear(0xFF000000);
				}



			} else if (!tronGame.finished()) {

				// move tronPlayers
				tronGame.move();
				tronGame.updateSpecials();
				tronGame.draw(this);
				tronGame.getCollidedPlayers();

				if (tronGame.finished()) tronGame.stop();

			} else {


				gameCanvas.decolorize(TronSettings.GAME_END_VISUAL_DECOLORIZE_SPEED,
						TronSettings.GAME_END_VISUAL_DECOLORIZE_MIN_SATURATION);
				float x = tronGame.getWinner().getX();
				float y = tronGame.getWinner().getY();
				int color = tronGame.getWinner().getTextColor();
				addSmears(Smear.createSmearDot(x,y,color,0));
				double rd = 2*Math.PI*(frameCount%15)/15.0f;
				addSmears(Smear.createSmearDot(x+(float)(10*Math.cos(rd)),y+(float)(10*Math.sin(rd)),color,0));

			}


			// draw smear on canvas and this
			for (int i=0;i<smears.size();i++) {
				smears.elementAt(i).moveAndDraw(this,tronGame.finished()?null:gameCanvas);
				if (smears.elementAt(i).hasDied()) smears.removeElementAt(i);
				i--;
			}

			if (tronGameIntro.introFinished()) tronGame.specialsDraw(this);
			// draw score:

			textFont(font18);
			tronGame.drawScore(this);

			//tronControl.repaintCanvas();

			if (tronGame.finished() && tronGame.endTimerHasFinished()) {
				quitGame();
				gameMode=MODE_DEMO;
			}
		}
	}

	public GImage getGameCanvas() {
		return gameCanvas;
	}

}
