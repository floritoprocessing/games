package graf.tron.special;

import java.util.Vector;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;
import graf.pimage.GImage;
import graf.tron.Smear;
import graf.tron.TronPlayer;
import graf.tron.TronSettings;




/**
 * Ideas for Special:<BR>
 * <table border=1>
 * <tr>
 * <td><strong>Type</strong></td>
 * <td><strong>Effect on</strong></td>
 * </tr>
 * <tr valign=top>
 * <td> <strong>Speed boost</strong> - Faster driving for an amount of time </td>
 * <td> Player only
 * </tr>
 * <tr valign=top>
 * <td> Shoot - Shoot one thing and destroy path</td>
 * <td> PTron.gameCanvas, TronGame.occupied
 * </tr>
 * <tr valign=top>
 * <td> <strong>Chaos</strong> - One random turn for enemies</td>
 * <td> all other Players
 * </tr>
 * <tr valign=top>
 * <td> Invincibility - Invincible for a short amount of time</td>
 * <td> Player only
 * </tr>
 * <tr valign=top>
 * <td> <strong>Freeze</strong> - Freeze others for a short amount of time</td>
 * <td> all other players
 * </tr>
 * <tr valign=top>
 * <td> Auto pilot - auto evasion maneuvre </td>
 * <td> Player only </td>
 * </tr>
 * </table>
 * @author mgraf
 *
 */
public class SpecialDefault {
	
	public static final String[] NAME = {
		"Speed boost",
		"Chaos",
		"Freeze",
		"Invincible"
	};
	public static final int SPECIAL_SURPRISE = -1;
	public static final int SPECIAL_SPEED_BOOST = 0;
	public static final int SPECIAL_CHAOS = 1;
	public static final int SPECIAL_FREEZE =2;
	public static final int SPECIAL_INVINCIBLE = 3;
	
	private static final int min=0, max=3;
	
	public static SpecialDefault create(int x, int y, GImage gameCanvas, PGraphics occupied, TronPlayer[] playerList,int playerId,int type) {
		if (type==SPECIAL_SURPRISE) 
			return create(x,y,gameCanvas, occupied, playerList, playerId, min+(int)Math.round(Math.random()*(max-min)));
		else if (type==SPECIAL_SPEED_BOOST) return new SpeedBoost(x,y,type,3000,null,null,playerList,playerId);
		else if (type==SPECIAL_CHAOS) return new Chaos(x,y,type,0,null,null,playerList,playerId);
		else if (type==SPECIAL_FREEZE) return new Freeze(x,y,type,3000,null,null,playerList,playerId);
		else if (type==SPECIAL_INVINCIBLE) return new Invincible(x,y,type,3000,null,null,playerList,playerId);
		return null;
	}
	
	protected final int type;
	protected final GImage gameCanvas;
	protected final PGraphics occupied;
	protected final int x, y;
	protected final TronPlayer[] playerList;
	
	protected int playerId;
	/**
	 * Time at which the method fire() has been called;
	 */
	private long fireTime;
	private final long activeTime;
	private boolean fired=false;
	private final Vector<Smear> smearCircle = new Vector<Smear>();
	
	public SpecialDefault(int x, int y, int type, long activeTime, GImage gameCanvas, PGraphics occupied, TronPlayer[] playerList, int playerId) {
		this.x=x;
		this.y=y;
		this.type=type;
		this.activeTime=activeTime;
		this.gameCanvas=gameCanvas;
		this.occupied=occupied;
		this.playerList=playerList;
		this.playerId=playerId;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setPlayerID(int id) {
		playerId=id;
	}
	
	public void fire() {
		fireTime=System.currentTimeMillis();
		fired=true;
	}
	
	public long getActiveTime() {
		return activeTime;
	}
	
	public long getRunningTime() {
		if (!fired) return 0;
		else return System.currentTimeMillis()-fireTime;
	}
	
	public void pickUp(TronPlayer tp) {
		
	}
	
	public int getType() {
		return type;
	}
	
	
	public String getTypeName() {
		return NAME[type];
	}
	
	public boolean isFired() {
		return fired;
	}

	
	public void draw(PApplet pa) {
		pa.ellipseMode(PApplet.CENTER);
		//pa.stroke(255,224,150,192); //0xffffe096
		pa.noStroke();
		pa.fill(64,50,10,192);
		pa.ellipse(x,y,TronSettings.SPECIAL_TRANSFER_DISTANCE*2,TronSettings.SPECIAL_TRANSFER_DISTANCE*2);
		
		for (float deg=0;deg<360;deg+=5) {
			float dir = (float)(deg/180.0f*Math.PI);
			Smear s = new Smear(x,y,dir,0xffffe096,-0.3f);
			s.shiftPosOutward(TronSettings.SPECIAL_TRANSFER_DISTANCE);
			smearCircle.add(s);
		}
		
		for (int i=0;i<smearCircle.size();i++) {
			smearCircle.elementAt(i).moveAndDraw(pa, null);
			if (smearCircle.elementAt(i).hasDied()) {
				smearCircle.removeElementAt(i);
				i--;
			}
		}
	}

	
	
	

}
