package graf.tron.special;

import graf.pimage.GImage;
import graf.tron.TronPlayer;
import graf.tron.TronSettings;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;

public class Chaos extends SpecialDefault implements Special {

	public Chaos(int x, int y,int type, long activeTime, GImage gameCanvas, PGraphics occupied, TronPlayer[] playerList, int playerId) {
		super(x,y,type,activeTime, gameCanvas, occupied, playerList, playerId);
	}

	public int getScore() {
		return TronSettings.SPECIAL_SCORE_CHAOS;
	}

	public void influence() {
		for (int i=0;i<playerList.length;i++) {
			if (i!=playerId)
				if (Math.random()<0.5) playerList[i].left(); else playerList[i].right();
		}
	}
	
	public boolean isActive() {
		return false;
	}

	public void stopInfluence() {
	}
	
	public void update() {
	}
	
	
	public void draw(PApplet pa) {
		super.draw(pa);
		pa.stroke(255,240,15);
		pa.noFill();
		pa.arc(x, y, TronSettings.SPECIAL_TRANSFER_DISTANCE*1.2f, TronSettings.SPECIAL_TRANSFER_DISTANCE*1.2f, 
				(float)(0.1*2*Math.PI), (float)(0.9*2*Math.PI));
	}

	public void drawActive(PApplet pa, float x, float y, int color) {
		// TODO Auto-generated method stub
		
	}

	

}
