package graf.tron.special;

import graf.pimage.GImage;
import graf.tron.TronPlayer;
import graf.tron.TronSettings;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;

public class Freeze extends SpecialDefault implements Special {

	float[] movX, movY;
	
	public Freeze(int x, int y, int type, long activeTime, GImage gameCanvas, PGraphics occupied, TronPlayer[] playerList, int playerId) {
		super(x,y,type, activeTime, gameCanvas, occupied, playerList, playerId);
	}

	public int getScore() {
		return TronSettings.SPECIAL_SCORE_FREEZE;
	}

	public void influence() {
		for (int i=0;i<playerList.length;i++) {
			if (i!=playerId) playerList[i].freeze();
		}
	}

	public boolean isActive() {
		return getRunningTime()<getActiveTime();
	}

	public void stopInfluence() {
		for (int i=0;i<playerList.length;i++)
			if (i!=playerId) playerList[i].unFreeze();
	}

	public void update() {
	}

	public void draw(PApplet pa) {
		super.draw(pa);
		pa.stroke(255,240,15);
		pa.noFill();
		float wid = TronSettings.SPECIAL_TRANSFER_DISTANCE*0.6f;
		float hei = TronSettings.SPECIAL_TRANSFER_DISTANCE*1.2f;
		pa.line(x-wid/2,y-hei/2,x-wid/2,y+hei/2);
		pa.line(x-wid/2,y-hei/2,x+wid/2,y-hei/2);
		pa.line(x-wid/2,y,x+wid*0.25f,y);
	}

	public void drawActive(PApplet pa, float x, float y, int color) {
		// TODO Auto-generated method stub
		
	}
	
}
