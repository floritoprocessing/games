/**
 * 
 */
package graf.tron.timer;

public class TimerEvent {
	
	private final String name;
	private final TimerEventListener timerEventListener; 
	
	private long oneShotTime;
	private long startTime;
	private long endTime;
	
	private final boolean oneShot;
	private boolean finishedFiring = false;
	private final boolean hasTimerEventListener;
	
	public TimerEvent(String eventName, long oneShotTime) {
		this.name=eventName;
		timerEventListener = null;
		this.oneShotTime=oneShotTime;
		oneShot=true;
		hasTimerEventListener=false;
	}
	
	public TimerEvent(String eventName, long startTime, long endTime) {
		this.name=eventName;
		timerEventListener = null;
		this.startTime=startTime;
		this.endTime=endTime;
		oneShot=false;
		hasTimerEventListener=false;
	}
	
	public TimerEvent(TimerEventListener timerEventListener, long oneShotTime) {
		this.name="";
		this.timerEventListener = timerEventListener;
		this.oneShotTime=oneShotTime;
		oneShot=true;
		hasTimerEventListener=true;
	}
	
	public TimerEvent(TimerEventListener timerEventListener, long startTime, long endTime) {
		this.name="";
		this.timerEventListener = timerEventListener;
		this.startTime=startTime;
		this.endTime=endTime;
		oneShot=false;
		hasTimerEventListener=true;
	}
	
	public String getName() {
		if (timerEventListener!=null) return timerEventListener.getName();
		else return name;
	}
	
	public void resetFiring() {
		finishedFiring = false;
	}
	
	public boolean hasTimerEventListener() {
		return hasTimerEventListener;
	}
	
	public TimerEventListener getTimerEventListener() {
		return timerEventListener;
	}
	
	public boolean isFiring(long currentTime) {
		if (!finishedFiring) {
			if (oneShot) {
				if (currentTime>=oneShotTime) {
					finishedFiring=true;
					return true;
				}
			} else {
				if (currentTime>=startTime&&endTime>currentTime) {
					return true;
				} else if (currentTime>=endTime) {
					finishedFiring=true;
					return true;
				}
			}
			return false;
		}
		return false;
	}

	public float getFiringPercentage(long currentTime) {
		if (oneShot) return 0;
		else return ((float)currentTime-startTime)/(endTime-startTime);
	}
	
	
}