package graf.tron.timer;

public interface TimerEventListener {

	public String getName();
	
	public void update(float firingPercentage);
	
}
