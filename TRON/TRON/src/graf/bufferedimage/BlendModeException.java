package graf.bufferedimage;

public class BlendModeException extends Exception {

	private static final long serialVersionUID = -2179210447165564315L;

	public BlendModeException(String msg) {
		super(msg);
	}
}
