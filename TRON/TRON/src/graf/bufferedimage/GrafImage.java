package graf.bufferedimage;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.WritableRaster;
import java.util.Hashtable;
import java.util.Vector;

public class GrafImage extends BufferedImage {

	public GrafImage(int arg0, int arg1, int arg2) throws GrafImageException {
		super(arg0, arg1, arg2);
		if (arg2!=BufferedImage.TYPE_INT_RGB) throw new GrafImageException("Only type TYPE_INT_RGB allowed");
	}

	public GrafImage(int arg0, int arg1, int arg2, IndexColorModel arg3) throws GrafImageException {
		super(arg0, arg1, arg2, arg3);
		if (arg2!=BufferedImage.TYPE_INT_RGB) throw new GrafImageException("Only type TYPE_INT_RGB allowed");
	}

	public GrafImage(ColorModel arg0, WritableRaster arg1, boolean arg2, Hashtable<?, ?> arg3) throws GrafImageException {
		super(0,0,0);
		throw new GrafImageException("Illegal constructor");
	}

	private int mode;
	public static final int MODE_COPY = 0;
	public static final int MODE_ADD = 1;

	public void setBlendMode(int mode) throws BlendModeException {
		if (mode!=MODE_COPY && mode!=MODE_ADD) throw new BlendModeException("Blend mode unknown!");
		this.mode = mode;
	}

	public int getBlendMode() {
		return mode;
	}



	public void setRGB(int x, int y, int rgb) {
		if (x<0||x>=getWidth()||y<0||y>=getHeight()) return;
		if (mode==MODE_COPY)
			super.setRGB(x,y,rgb);
		else if (mode==MODE_ADD) 
			super.setRGB(x,y,blend(rgb,getRGB(x,y),mode));
	}

	public void setRGB(int x, int y, int rgb, float opac) {
		if (x<0||x>=getWidth()||y<0||y>=getHeight()) return;
		if (mode==MODE_COPY)
			super.setRGB(x,y,blend(rgb,opac,getRGB(x,y),mode));
		else if (mode==MODE_ADD)
			super.setRGB(x,y,blend(rgb,opac,getRGB(x,y),mode));
	}

	public void setRGB(float x, float y, int rgb, float opac) {
		int x0=(int)x, x1=x0+1;
		int y0=(int)y, y1=y0+1;
		float px1 = x-x0, px0=1-px1;
		float py1 = y-y0, py0=1-py1;
		float p00 = opac*px0*py0;
		float p10 = opac*px1*py0;
		float p01 = opac*px0*py1;
		float p11 = opac*px1*py1;
		setRGB(x0,y0,rgb,p00);
		setRGB(x1,y0,rgb,p10);
		setRGB(x0,y1,rgb,p01);
		setRGB(x1,y1,rgb,p11);
	}

	public void setRGB(float x, float y, int rgb) {
		setRGB(x,y,rgb,1.0f);
	}






	public static final int blend(int rgb1, int rgb2, int mode) {
		if (mode==MODE_ADD) {
			return maxcolor(red(rgb1)+red(rgb2), green(rgb1)+green(rgb2), blue(rgb1)+blue(rgb2));
		}
		return 0;
	}

	public static final int blend(int rgb1, float opac1, int rgb2, int mode) {
		if (mode==MODE_COPY) {
			float opac2=1-opac1;
			return maxcolor(opac1*red(rgb1)+opac2*red(rgb2),
					opac1*green(rgb1)+opac2*green(rgb2),
					opac1*blue(rgb1)+opac2*blue(rgb2));
		}
		if (mode==MODE_ADD) {
			return maxcolor(opac1*red(rgb1)+red(rgb2), opac1*green(rgb1)+green(rgb2), opac1*blue(rgb1)+blue(rgb2));
		}
		return 0;
	}

	public static final int maxcolor(float r, float g, float b) {
		return max(r)<<16|max(g)<<8|max(b);
	}

	public static final int maxcolor(int r, int g, int b) {
		return max(r)<<16|max(g)<<8|max(b);
	}

	public static final int color(int r, int g, int b) {
		return r<<16|g<<8|b;
	}

	public static final int max(float v) {
		return (int)(v>255?255:v);
	}

	public static final int red(int rgb) {
		return rgb>>16&0xff;
	}

	public static final int green(int rgb) {
		return rgb>>8&0xFF;
	}

	public static final int blue(int rgb) {
		return rgb&0xFF;
	}

	public static final int LINE_MODE_EXCLUSIVE = 0;
	public static final int LINE_MODE_INCLUSIVE = 1;

	public Vector<float[]> lineRGB(int x0, int y0, int x1, int y1, int rgb, int lineMode) {
		return lineRGB(x0, y0, x1, y1, rgb, 1, lineMode);
	}
	
	public Vector<float[]> lineRGB(int x0, int y0, int x1, int y1, int rgb, float opacity, int lineMode) {
		if (lineMode!=LINE_MODE_EXCLUSIVE&&lineMode!=LINE_MODE_INCLUSIVE) throw new RuntimeException("Unknown line mode");

		double dx = x1-x0, dy=y1-y0;
		Vector<float[]> out = new Vector<float[]>();
		
		if (x0==x1 && y0==y1) {
			setRGB(x0,y0,rgb,opacity);
			out.add(new float[] {x0,y0});
			return out;
		}

		if (x0==x1) {
			double d = Math.abs(y1-y0) + lineMode;
			for (float fd=0;fd<d;fd+=1) {
				double p = fd/d;
				int y = (int)(y0+dy*p);
				setRGB(x0, y, rgb, opacity);
				out.add(new float[] {x0,y});
			}
			return out;
		}

		else if (y0==y1) {
			double d = Math.abs(x1-x0) + lineMode;
			for (float fd=0;fd<d;fd+=1) {
				double p = fd/d;
				int x = (int)(x0+dx*p);
				setRGB(x, y0, rgb, opacity);
				out.add(new float[] {x,y0});
			}
			return out;
		}

		else {
			double d = Math.sqrt(dx*dx+dy*dy) + lineMode;		
			for (float fd=0;fd<d;fd+=1) {
				double p = fd/d;
				float x=(float)(x0+dx*p),y=(float)(y0+dy*p); 
				setRGB(x, y, rgb, opacity);
				out.add(new float[] {x,y});
			}
			return out;
		}

	}



}
