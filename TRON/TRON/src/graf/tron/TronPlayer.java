package graf.tron;

public class TronPlayer {

	private String name;
	private float x, y;
	private float lastX, lastY;
	private float movX, movY;
	private float speed = 0.1f; // pixel per ms
	private int color;
	private boolean alive = true;

	public TronPlayer(TronGame game, String name, int color, float x, float y) {
		this.x=x;
		this.y=y;
		lastX = x;
		lastY = y;
		if (Math.random()<0.5) {
			movX=0;
			movY=(y<game.getHeight()/2?speed:-speed);
		} else {
			movX=(x<game.getWidth()/2?speed:-speed);
			movY=0;
		}
		this.name = name;
		this.color = color;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public float getMovX() {
		return movX;
	}
	
	public float getMovY() {
		return movY;
	}
	
	public String getName() {
		return name;
	}

	public void move(double milliseconds) {
		if (!alive) return;
		lastX = x;
		lastY = y;
		x += milliseconds*movX;
		y += milliseconds*movY;
	}

	public void left() {
		if (!alive) return;
		if (movX==speed) {
			movX=0;
			movY=-speed;
		}
		else if (movY==-speed) {
			movY=0;
			movX=-speed;
		}
		else if (movX==-speed) {
			movX=0;
			movY=speed;
		}
		else if (movY==speed) {
			movY=0;
			movX=speed;
		}
	}

	public void right() {
		if (!alive) return;
		if (movX==speed) {
			movX=0;
			movY=speed;
		}
		else if (movY==speed) {
			movY=0;
			movX=-speed;
		}
		else if (movX==-speed) {
			movX=0;
			movY=-speed;
		}
		else if (movY==-speed) {
			movY=0;
			movX=speed;
		}
	}

	public void draw(TronCanvas tronCanvas) {
		if (!alive) return;	
		tronCanvas.tronLine((int)x,(int)y,(int)lastX,(int)lastY,color);
	}

	public void kill() {
		alive  = false;
	}



}
