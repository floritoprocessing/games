package graf.tron;

import graf.bufferedimage.GrafImage;

import java.util.List;
import java.util.Vector;

import javax.swing.SwingWorker;

public class TronTask extends SwingWorker<Void, TronStats> {
	
	private static final int FPS = 20;
	private static final int MSPF = 1000/FPS;

	private final TronCanvas tronCanvas;
	private final TronOccupiedCanvas occupied;
	private final TronGame tronGame;
	private long lastFrame = System.nanoTime();
	
	public TronTask(TronCanvas tronCanvas, TronOccupiedCanvas occupied, TronGame tronGame) {
		this.tronCanvas = tronCanvas;
		this.occupied = occupied;
		this.tronGame = tronGame;
	}

	@Override
	protected Void doInBackground() {
		while (!isCancelled()) {
			publish(new TronStats((System.nanoTime()-lastFrame)/1000000.0));
			lastFrame = System.nanoTime();
			try {
				Thread.sleep(MSPF);
			} catch (InterruptedException e) {}
		}
		return null;
	}
	
	@Override
    protected void process(List<TronStats> stats) {
		double ms = stats.get(stats.size()-1).getFrameTimeMs();
		if (!tronGame.introFinished()) {
			tronCanvas.setBackbufferBlendMode(GrafImage.MODE_COPY);
			tronGame.intro(tronCanvas,ms);
			tronGame.draw(tronCanvas);
		} else if (!tronGame.finished()) {
			tronCanvas.setBackbufferBlendMode(GrafImage.MODE_ADD);
			tronGame.move(ms);
			Vector<TronPlayer> deadPlayers = tronGame.checkAndGetCollidedPlayers(occupied);
			tronGame.draw(tronCanvas);
		} else {
			System.out.println("FINISHED");
		}
		tronCanvas.repaint(ms,occupied.isVisible());
    }


}
