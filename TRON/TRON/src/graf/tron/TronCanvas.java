package graf.tron;

import graf.bufferedimage.BlendModeException;
import graf.bufferedimage.GrafImage;
import graf.bufferedimage.GrafImageException;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.Vector;

public class TronCanvas extends Canvas {

	private static final long serialVersionUID = 5872519959228887067L;

	private GrafImage backbuffer;
	private Graphics backg;
	private final int width, height;
	
	private TronOccupiedCanvas tronOccupiedCanvas;
	private BufferedImage occupied;
	private Graphics occupiedGraphics;

	private Vector<Splat> splats;

	private class Splat {
		
		private final float x, y, maxSize, growSpeed;
		private final int color;
		private final float opacity;
		private float size=0;

		public Splat(float x, float y, float maxSize, float growSpeed, int color, float opacity) {
			this.x=x;
			this.y=y;
			this.maxSize=maxSize;
			this.growSpeed=growSpeed;
			this.color = color;
			this.opacity = opacity;
		}

		public boolean growToMax(double ms) {
			size += ms*growSpeed;
			if (size>maxSize) return true;
			return false;
		}

		public void draw(GrafImage img, BufferedImage occupied) {
			for (float xo=-size;xo<=size;xo++) for (float yo=-size;yo<=size;yo++) {
				img.setRGB(x+xo, y+yo, color, opacity);
				int sx=(int)(x+xo), sy=(int)(y+yo);
				if (sx>=0&&sx<occupied.getWidth()&&sy>=0&&sy<occupied.getHeight())
					occupied.setRGB(sx, sy, color);
			}
		}

	}

	public TronCanvas(int width, int height, TronOccupiedCanvas tronOccupiedCanvas) {
		this.width = width;
		this.height = height;
		setPreferredSize(new Dimension(width,height));
		try {
			backbuffer = new GrafImage(width,height,BufferedImage.TYPE_INT_RGB);
		} catch (GrafImageException e) {
			System.exit(0);
		}
		backg = backbuffer.getGraphics();
		splats = new Vector<Splat>();
		
		this.tronOccupiedCanvas = tronOccupiedCanvas;
		occupied = tronOccupiedCanvas.getBackbufferImage();
		occupiedGraphics = occupied.getGraphics();
	}

	public void clear() {
		backg.setColor(Color.black);
		backg.fillRect(0, 0, width, height);
		occupiedGraphics.setColor(Color.black);
		occupiedGraphics.fillRect(0, 0, width, height);
		splats = new Vector<Splat>();
		repaint();
	}

	public void setBackbufferBlendMode(int mode) {
		try {
			backbuffer.setBlendMode(mode);
		} catch (BlendModeException e) {
			e.printStackTrace();
		}
	}

	public Graphics getBackBufferGraphics() {
		return backg;
	}

	public GrafImage getBackBufferImage() {
		return backbuffer;
	}
	
	public BufferedImage getOccupied() {
		return occupied;
	}

	public void update(Graphics g) {
		g.drawImage(backbuffer,0,0,null);
	}

	public void paint(Graphics g) {
		update(g);
	}

	public void tronLine(int x0, int y0, int x1, int y1, int rgb) {
		Vector<float[]> linedots = backbuffer.lineRGB(x0, y0, x1, y1, rgb, 1.0f, GrafImage.LINE_MODE_EXCLUSIVE);
		for (int i=0;i<linedots.size();i++) {
			float x = linedots.elementAt(i)[0];
			float y = linedots.elementAt(i)[1];
			if (x>=0&&x<width&&y>=0&&y<height)
				occupied.setRGB((int)x,(int)y, rgb);
			splats.add(new Splat(x,y,
					TronPlayerSettings.SIZE,TronPlayerSettings.GROW_SPEED,rgb,TronPlayerSettings.OPACITY));
		}
	}

	public void repaint(double ms, boolean repaintOccupied) {
		if (splats!=null) {
			for (int i=0;i<splats.size();i++) {
				if (splats.elementAt(i).growToMax(ms)) {
					splats.remove(splats.elementAt(i));
					i--;
				} else {
					splats.elementAt(i).draw(backbuffer,occupied);
				}
			}		
		}
		if (repaintOccupied) tronOccupiedCanvas.repaint();
		this.repaint();
	}


}
