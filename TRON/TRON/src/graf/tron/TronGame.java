package graf.tron;

import java.awt.event.KeyEvent;
import java.util.Vector;

public class TronGame {

	private int width, height;
	private TronPlayer[] tronPlayer;
	private boolean introFinished = false;
	private boolean gameFinished = false;

	private static final float INTRO_TIME = 3000;
	private static final float INTRO_GRID_TIME = 1000;
	private static final float INTRO_PREPARE_TIME = 2000;
	private float introTime = INTRO_TIME; // ms

	public TronGame(int width, int height) {
		this.width = width;
		this.height = height;
		tronPlayer = new TronPlayer[2];

		double minDistance = Math.sqrt(width*width+height*height)/4.0;
		boolean tooClose = true;

		while (tooClose) {
			for (int i=0;i<tronPlayer.length;i++) {
				float x = (float)(width*(0.1+0.8*Math.random()));
				float y = (float)(height*(0.1+0.8*Math.random()));
				tronPlayer[i] = new TronPlayer(this,"Player "+(i+1),TronPlayerSettings.color[i],x,y);
			}
			
			tooClose=false;
			for (int i=0;i<tronPlayer.length-1;i++) {
				for (int j=i+1;j<tronPlayer.length;j++) {
					float dx = tronPlayer[i].getX()-tronPlayer[j].getX();
					float dy = tronPlayer[i].getY()-tronPlayer[j].getY();
					double distance = Math.sqrt(dx*dx+dy*dy);
					if (distance<minDistance) tooClose=true;
				}
			}
		}



	}

	public void intro(TronCanvas tronCanvas, double frameTimeMs) {
		introTime-=frameTimeMs;
		if (introTime<0) {
			introTime=0;
			introFinished=true;
		}

		float negIntroPercentage = (float)(introTime-INTRO_PREPARE_TIME)/INTRO_GRID_TIME;
		if (negIntroPercentage<0) negIntroPercentage=0;		
		TronBackground.draw(tronCanvas,negIntroPercentage);
	}

	public void move(double milliseconds) {
		for (int i=0;i<tronPlayer.length;i++) 
			tronPlayer[i].move(milliseconds);
	}

	public void draw(TronCanvas tronCanvas) {
		for (int i=0;i<tronPlayer.length;i++) 
			tronPlayer[i].draw(tronCanvas);
	}

	public void keyPressed(KeyEvent arg0) {
		int kc = arg0.getKeyCode();
		for (int i=0;i<tronPlayer.length;i++) {
			if (kc==TronPlayerSettings.steeringKey[i][0])
				tronPlayer[i].left();
			else if (kc==TronPlayerSettings.steeringKey[i][1])
				tronPlayer[i].right();
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public boolean introFinished() {
		return introFinished ;
	}

	public boolean finished() {
		return gameFinished;
	}

	public Vector<TronPlayer> checkAndGetCollidedPlayers(TronOccupiedCanvas occupied) {
		
		Vector<TronPlayer> out = new Vector<TronPlayer>();
		
		for (int i=0;i<tronPlayer.length;i++) {
			float x = tronPlayer[i].getX();
			float y = tronPlayer[i].getY();
			int width = occupied.getWidth();
			int height = occupied.getHeight();
			if (x<0||x>=width||y<0||y>=height) {
				tronPlayer[i].kill();
				out.add(tronPlayer[i]);
			}
		}
		
		if (out.size()==tronPlayer.length) gameFinished = true;
		return out;
	}




}
