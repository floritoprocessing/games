package graf.tron;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;

public class Tron extends JFrame implements ActionListener, KeyListener, WindowListener {
	
	private static final long serialVersionUID = 1L;
	
	private TronTask tronTask;
	private final TronCanvas tronCanvas;
	private final TronOccupiedCanvas tronOccupiedCanvas;
	private TronGame tronGame;
	
	private final JButton newGameButton = new JButton("New Game");
	private final JButton endGameButton = new JButton("End Game");
	private final JToggleButton occupiedVisibleButton = new JToggleButton("Occupied visible");
		
	public Tron() {
		super("Tron");
		setLayout(new GridBagLayout());
		addWindowListener(this);
		setFocusable(true);
		
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.CENTER;
		c.insets = new Insets(5,10,5,10);
		c.gridx=0;
		c.gridy=0;
		newGameButton.addActionListener(this);
		getContentPane().add(newGameButton,c);
		c.gridx=1;
		endGameButton.addActionListener(this);
		endGameButton.setEnabled(false);
		getContentPane().add(endGameButton,c);
		c.gridx=2;
		occupiedVisibleButton.addActionListener(this);
		getContentPane().add(occupiedVisibleButton,c);
		c.gridx=0;
		c.gridy=1;
		c.gridwidth=4;
		tronOccupiedCanvas = new TronOccupiedCanvas(1100,250);
		tronOccupiedCanvas.setVisible(false);
		tronCanvas = new TronCanvas(1100,250,tronOccupiedCanvas);
		tronCanvas.addKeyListener(this);
		getContentPane().add(tronCanvas,c);
		c.gridy=2;
		getContentPane().add(tronOccupiedCanvas,c);
		pack();
		setResizable(false);
		setVisible(true);
	}
	
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource().equals(newGameButton)) {
			newGameButton.setEnabled(false);
			endGameButton.setEnabled(true);
			tronGame = new TronGame(1100,250);
			tronCanvas.requestFocus();
			tronCanvas.clear();
			(tronTask = new TronTask(tronCanvas,tronOccupiedCanvas,tronGame)).execute();
		}
		else if (arg0.getSource().equals(endGameButton)) {
			newGameButton.setEnabled(true);
			endGameButton.setEnabled(false);
			tronTask.cancel(true);
			tronTask = null;
			tronGame = null;
		}
		else if (arg0.getSource().equals(occupiedVisibleButton)) {
			tronOccupiedCanvas.setVisible(occupiedVisibleButton.isSelected());
			pack();
		}
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	new Tron();
            }
		});
	}

	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowClosing(WindowEvent arg0) {
		System.exit(0);
	}

	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void keyPressed(KeyEvent arg0) {
		if (tronGame!=null) {
			tronGame.keyPressed(arg0);
		}
	}

	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
	
}
