package graf.tron;

import graf.bufferedimage.GrafImage;

import java.awt.Color;
import java.awt.Graphics;

public class TronBackground {

	private static final int grid = 30;
	private static final int gridColor = 0xeaf2cb;
	private static final float gridOpacity = 0.5f;

	public static void draw(TronCanvas tronCanvas, float negIntroPercentage) {

		float introPercentage = 1-negIntroPercentage;

		if (negIntroPercentage==0) {
			tronCanvas.clear();
			drawFullGrid(tronCanvas);
		} else {

			int width = tronCanvas.getWidth();
			int height = tronCanvas.getHeight();

			GrafImage bg = tronCanvas.getBackBufferImage();
			Graphics g = tronCanvas.getBackBufferGraphics();
			int val = (int)(255*Math.pow(negIntroPercentage,8));
			float opac = gridOpacity*introPercentage;
			float opac2 = opac*0.5f;
			g.setColor(new Color(val,val,val));
			g.fillRect(0, 0, bg.getWidth(), bg.getHeight());
			for (int x=0;x<width;x+=grid) {
				for (int y=0;y<height*introPercentage;y++) {
					bg.setRGB(x-1, y, gridColor, opac2);
					bg.setRGB(x, y, gridColor, opac);
					bg.setRGB(x+1, y, gridColor, opac2);
				}
			}
			for (int y=0;y<height;y+=grid) {
				for (int x=0;x<width*introPercentage;x++) {
					bg.setRGB(x, y-1, gridColor, opac2);
					bg.setRGB(x, y, gridColor, opac);
					bg.setRGB(x, y+1, gridColor, opac2);
				}
			}
		}
	}

	public static void drawFullGrid(TronCanvas tronCanvas) {
		int width = tronCanvas.getWidth();
		int height = tronCanvas.getHeight();
		float opac = gridOpacity;
		float opac2 = gridOpacity*0.5f;
		GrafImage bg = tronCanvas.getBackBufferImage();

		for (int x=0;x<width;x+=grid) {
			for (int y=0;y<height;y++) {
				bg.setRGB(x-1, y, gridColor, opac2);
				bg.setRGB(x, y, gridColor, opac);
				bg.setRGB(x+1, y, gridColor, opac2);
			}
		}
		for (int y=0;y<height;y+=grid) {
			for (int x=0;x<width;x++) {
				bg.setRGB(x, y-1, gridColor, opac2);
				bg.setRGB(x, y, gridColor, opac);
				bg.setRGB(x, y+1, gridColor, opac2);
			}
		}
	}

}
