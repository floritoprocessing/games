package graf.tron;

import java.awt.event.KeyEvent;

public class TronPlayerSettings {
	
	public static final float SIZE = 5;
	public static final float GROW_SPEED  = 0.05f;
	public static final float OPACITY = 0.5f;
	
	public static final int[] color = {
		0xf0200e,
		0x020ef0,
		0x0ef002
	};
	
	public static final int[][] steeringKey = {
		{ KeyEvent.VK_A, KeyEvent.VK_S },
		{ KeyEvent.VK_D, KeyEvent.VK_F },
		{ KeyEvent.VK_G, KeyEvent.VK_H }
	};
}
