package graf.tron;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class TronOccupiedCanvas extends Canvas {

	private int width, height;
	private BufferedImage backbuffer;
	private Graphics backg;
	
	public TronOccupiedCanvas(int width, int height) {
		this.width = width;
		this.height = height;
		setPreferredSize(new Dimension(width,height));
		backbuffer = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
		backg = backbuffer.getGraphics();
	}
	
	public Graphics getBackbufferGraphics() {
		return backg;
	}
	
	public BufferedImage getBackbufferImage() {
		return backbuffer;
	}
	
	public void update(Graphics g) {
		g.drawImage(backbuffer,0,0,null);
	}
	
	public void paint(Graphics g) {
		update(g);
	}

}
